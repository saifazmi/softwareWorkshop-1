import java.util.Scanner;
import java.util.ArrayList; //to use ArrayList and its calsses

public class Week2{
	
	public static void main(String[] args){
		
		Scanner input = new Scanner (System.in);
		ArrayList<String> list = new ArrayList<String>(); //declaring a new ArrayList
		
		System.out.println("Please input a string to continue (or STOP to terminate): ");
		String userWord = input.nextLine();
		
		userWord = userWord.toLowerCase();
		
		while(!userWord.equals("stop")){
			
			list.add(userWord); //add() is use to add elements of type list to the ArrayList
			userWord = input.nextLine();
			
			userWord = userWord.toLowerCase();
		}
		
		System.out.println("\nPrinting it all out - ");
		
		for(int i = 0; i < list.size(); i++){	//size() is an ArrayList function
			System.out.println(list.get(i));
		}
		
		//System.out.println("hello".compareTo("goodbye")); //return -ve/0/+ve depending upon the strings being compared.
		//it is a String class function.
		//looks through the list prints the first alphabetical
		
		//System.out.println("Printing Alphabeically");
		
		String smallest = list.get(0);
		
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).compareTo(smallest) < 0){
				smallest = list.get(i);
			}
		}
		
		System.out.println("\nSmallest is: " + smallest);
		
		System.out.println("\nThose which come before M - ");
		
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).compareTo("m") > 0){
				System.out.println(list.get(i));
			}
		}
		
		System.out.print("\nNumber of times apple appears in List: ");
		
		int appleCount = 0;
		
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).equals("apple")){
				appleCount++;
			}
		}
		
		System.out.println(appleCount);
		
		if(list.contains("pear")){
		
			int n = list.indexOf("pear");
			System.out.println("\nIndex of pear in the list = " + n);
			//indexOf() returns -1 if there are no pears in the list
			//but it will crash if u dont have the Element in the string - Index out of Bound err
			
			System.out.println("\nSo we found Pear");
			System.out.println("I dont like it, so lets just remove() it :D");
			
			list.remove(n); //Caution! - Index out of Bound possible
			//list.remove("pear");
			
			System.out.println("\nLet's see if I got rid of it - ");
			
			for(int i = 0; i < list.size(); i++){
				System.out.println(list.get(i));
			}
			
			System.out.println("\nVola!");
		}
		
		else{
		
			System.out.println("\nThe list contains no Pears :( ");
		}
		
		//Lets remove all Banana's
		
		System.out.println("\nLet's remove all banana's - ");
		
		while(list.contains("banana")){
			list.remove("banana");
		}
		
		for(int i = 0; i < list.size(); i++){
			System.out.println(list.get(i));
		}
			
		System.out.println("\nHi5!");
	}
}
