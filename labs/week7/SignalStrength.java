import javax.swing.JFrame;

public class SignalStrength{

	public static void main(String[] args) {
		
		int mapSize = 10;
		int frameSize = 800;
		double maxSignal = 300.0;

		JFrame frame= new JFrame("Map of network signal strength");

		frame.setSize(frameSize, frameSize);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Transmitter t1 = new Transmitter(1.0, 1.0, 80.0);
		Transmitter t2 = new Transmitter(8.0, 2.0, 100.0);
		Transmitter t3 = new Transmitter(7.5, 7.0, 150.0);
		Transmitter t4 = new Transmitter(7.5, 1.0, 200.0);
		Transmitter t5 = new Transmitter(7.5, 6.0, 275.0);
		Transmitter t6 = new Transmitter(8.0, 1.2, 250.0);

		Network net = new Network();
		net.add(t1);
		net.add(t2);
		net.add(t3);
		net.add(t4);
		net.add(t5);
		net.add(t6);

		SignalComponent comp = new SignalComponent(net, mapSize, frameSize, maxSignal);
		frame.add(comp);

		frame.setVisible(true);
	}
}