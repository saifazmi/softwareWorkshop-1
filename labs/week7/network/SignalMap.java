/**
Represents a map of signal over a grid
*/
public class SignalMap{

	private boolean[][] map;
	private double threshold;
	private int size;
	private Network network;

	/**
	Create a new signal map for the concerned netowrk.
	@param threshold the limit for poor signal
	@param size the size of the map
	@param network the netowrk for which map has to be drawn
	*/
	public SignalMap(double threshold, int size, Network network){

		this.threshold = threshold;
		this.size = size;
		this.network = network;
		this.map = new boolean[size][size];

		for(int i = 0; i < size; i++){

			for(int j = 0; j < size; j++){

				if(network.getSignal((double)i, (double)j) < threshold){

					map[i][j] = true;
				}

				else{

					map[i][j] = false;
				}
			}
		}
	}

	public String toString(){

		return "The size of the map is " + size + " unit(s)";
	}

	/**
	Display the map of signal strength across the grid, 
	where "X" represents poor and/or no signal and
	"O" represents strong signal
	*/
	public void display(){
		
		for(int i = 0; i < size; i++){

			for(int j = 0; j < size; j++){

				if(map[i][j]){
					
					System.out.print("\u001B[31mX ");
				}

				else{

					System.out.print("\u001B[34mO ");
				}
			}

			System.out.println("\u001B[m");
		}
	}

	/**
	Fraction of poor signal across the map
	@return the fraction of poor signals
	*/
	public double poorSignal(){

		int poorSignal = 0;

		for(int i = 0; i < size; i++){

			for(int j = 0; j < size; j++){

				if(map[i][j]){

					poorSignal++;
				}
			}
		}

		return (double)poorSignal/(size * size);
	}

	/**
	Draw a map comparing signal strength's across map 
	between the existing network and a new network
	@param newNet the new network
	*/
	public void compare(Network newNet){
		
		for(int i = 0; i < size; i++){
			
			for(int j = 0; j < size; j++){
				
				if(newNet.getSignal((double)i, (double)j) > network.getSignal((double)i, (double)j)){
				
					System.out.print("\u001B[34mO ");
				}
				else{

					System.out.print("\u001B[31mX ");
				}
			}

			System.out.println("\u001B[m");
		}	
	}	
}