import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Rectangle;

public class SignalComponent extends JComponent{

	private Network net;
	private int mapSize;
	private int frameSize;
	private double maxSignal;

	public SignalComponent(Network net, int mapSize, int frameSize, double maxSignal){

		super();

		this.net = net;
		this.mapSize = mapSize;
		this.frameSize = frameSize;
		this.maxSignal = maxSignal;
	}

	public void paintComponent(Graphics g){

		Graphics2D g2 = (Graphics2D)g;

		frameSize = getWidth();

		//g2.drawString("Test String", 50,50);

		for(int i = 0; i < frameSize; i++){

			for(int j = 0; j < frameSize; j++){

				// convert (i,j) to a map coordinate
				double x = mapSize * (double)i/frameSize;
				double y = mapSize * (double)j/frameSize;
				
				// clculate signal strength
				double signal = net.getSignal(x, y);
				
				try{
					//calculate the grey scale level
					int greyLevel = (int)(255 * (signal/maxSignal));

					// create a Color object
					Color grey = new Color(greyLevel, greyLevel, greyLevel);
					g2.setColor(grey);
				}

				catch(IllegalArgumentException e){

					g2.setColor(Color.RED);
				}

				// draw pixel
				Rectangle pixel = new Rectangle(i, j, 1, 1);
				g2.fill(pixel);
			}
		}
	}
}