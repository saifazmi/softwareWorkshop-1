import java.util.Scanner;

public class Week1{
	public static void main(String[] args){
		System.out.println("hello world");
		System.out.println("What is your name?");

		Scanner in = new Scanner(System.in);
		String name = in.nextLine();
		System.out.println("Hello " + name + " :))");

		System.out.println("What is your favourite colour?");
		String colour = in.nextLine();
		System.out.println("Your favourite colour is " + colour);

		int x = name.length();
		char c = name.charAt(0);

		System.out.println("Your name has " + x + " characters");
		System.out.println("The first charcter is " + c);

		int y = colour.length();
		char d = colour.charAt(y-1);
		
		System.out.println("The last character of " + colour + " is " + d);

		System.out.println("What is your age?");
		int age = in.nextInt(); in.nextLine();

		System.out.println("You are " + age + " years old");

		int newage = age + 1;
		int young = age - 10;
		int twice = age * 2;

		if(age < 25){
			System.out.println("You are young");
		}
		else{
			System.out.println("You are old");
		}
		
		System.out.println("Enter a number");
		int n = in.nextInt(); in.nextLine();
		
		if(n > 10 && n < 100){
			System.out.println("medium number");
		}
		else{
			System.out.println("too large or too small number");
		}

		System.out.println("Enter some text");
		String input = in.nextLine();

		while(input.length() > 0){
			input = in.nextLine();
		}

		System.out.println("Enter a number");
		int num = in.nextInt(); in.nextLine();

		while(num >= 0){
			num = in.nextInt(); in.nextLine();
		}
		
		num = 1;

		System.out.println("Printing numbers using WHILE loop");

		while(num <= 10){
			System.out.println(num);
			num++;
		}
		
		System.out.println("Printing numbers using FOR loop");
		
		for(int i = 1; i <= 10; i++){
			System.out.println(i);
		}
		
		System.out.println("Printing numbers in reverse using FOR loop");
		
		for(int i = 10; i > 0; i--){
			System.out.println(i);
		}
	}
}
