import javax.swing.JPanel;
import javax.swing.border.Border;
import java.awt.BorderLayout;

/**
 * @author : saif
 * @date : 02/12/14
 */
public class NoughtsCrossesComponent extends JPanel {

    public NoughtsCrossesComponent(NoughtsCrosses xoxo){

        // set layout to BorderLayout
        super(new BorderLayout());

        // create model
        NoughtsCrossesModel model = new NoughtsCrossesModel(xoxo);

        //create view
        BoardView board = new BoardView(model);

        // create control panel
        ButtonPanel buttons = new ButtonPanel(model);

        // make views observe model
        model.addObserver(board);

        // add view and controls
        add(board, BorderLayout.CENTER);
        add(buttons, BorderLayout.SOUTH);
    }
}
