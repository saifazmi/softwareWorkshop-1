import java.util.Observable;

public class NoughtsCrossesModel extends Observable {

    private NoughtsCrosses xoxo;

    public NoughtsCrossesModel(NoughtsCrosses xoxo) {

        super();
        this.xoxo = xoxo;
    }

    /**
     * Get symbol at given location
     *
     * @param i the row
     * @param j the column
     * @return the symbol at that location
     */
    public int get(int i, int j) {
        return xoxo.get(i, j);
    }


    /**
     * Is it cross's turn?
     *
     * @return true if it is cross's turn, false for nought's turn
     */
    public boolean isCrossTurn() {
        return xoxo.isCrossTurn();
    }

    /**
     * Let the player whose turn it is play at a particular location
     *
     * @param i the row
     * @param j the column
     */
    public void turn(int i, int j) {
        xoxo.turn(i, j);
        setChanged();
        notifyObservers();
    }

    /**
     * Determine who (if anyone) has won
     *
     * @return CROSS if cross has won, NOUGHT if nought has won, otherwise BLANK
     */
    public int whoWon() {
        return xoxo.whoWon();
    }

    /**
     * Start a new game
     */
    public void newGame() {
        xoxo.newGame();
        setChanged();
        notifyObservers();
    }
}
