import javax.swing.JFrame;

/**
 * @author : saif
 * @date : 02/12/14
 */
public class NoughtsCrossesGUI {

    public static void main(String[] args) {

        NoughtsCrosses xoxo = new NoughtsCrosses();

        NoughtsCrossesComponent comp = new NoughtsCrossesComponent(xoxo);

        JFrame frame = new JFrame("Do you wanna play game?");
        frame.setSize(400,400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.add(comp);

        frame.setVisible(true);
    }
}
