import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

public class BoardView extends JPanel implements Observer {

    private NoughtsCrossesModel model;
    private JButton[][] cell;

    public BoardView(NoughtsCrossesModel model) {

        super(new GridLayout(3, 3));
        this.model = model;
        cell = new JButton[3][3];

        for (int i = 0; i < 3; i++) {

            for (int j = 0; j < 3; j++) {

                cell[i][j] = new JButton(" ");
                cell[i][j].addActionListener(new ButtonListener(model, i, j));
                cell[i][j].setFocusable(false);
                add(cell[i][j]);
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {

        // for each square on board:
        for (int i = 0; i < 3; i++) {

            for (int j = 0; j < 3; j++) {

                if (model.get(i, j) == NoughtsCrosses.NOUGHT) {
                    cell[i][j].setText("O");
                    cell[i][j].setEnabled(false);
                } else if (model.get(i, j) == NoughtsCrosses.CROSS) {
                    cell[i][j].setText("X");
                    cell[i][j].setEnabled(false);
                } else {
                    cell[i][j].setText(" ");
                    boolean notOver =
                            (model.whoWon() == NoughtsCrosses.BLANK);
                    cell[i][j].setEnabled(notOver);
                }
            }
        }

        repaint();
    }
}
