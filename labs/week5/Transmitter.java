/**
Represents the location and power of a mobile phone transmitter.
*/
public class Transmitter{

	private double xCoord;
	private double yCoord;
	private double power;
	private final double limit = 1.0;

	/**
	Create a transmitter at a given location with given power
	@param xCoord The x coordinate
	@param yCoord The y coordinate
	@param power The power of the transmitter
	*/
	public Transmitter(double xCoord, double yCoord, double power){

		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.power = power;
	}

	public String toString(){

		return "Transmitter located at: " + xCoord + ", " + yCoord;
	}

	/**
	Find signal strength of mobile phone
	@param x The x coordinate of the phone
	@param y The y coordinate of the phone
	@return The signal strength at that location
	*/
	public double getSignal(double x, double y){

		//calculate distance
		double distance = Math.sqrt((xCoord - x) * (xCoord - x)
									+ (yCoord - y) * (yCoord - y));

		//calculate signal
		if(distance < limit){

			return power;
		}

		else {

			return power/(distance * distance);
		}
	}
}