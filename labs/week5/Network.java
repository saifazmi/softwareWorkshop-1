/**
Reprsents a netwrok of transmitters.
*/
import java.util.ArrayList;

//collection of transmitter Array lIST
//constructor atty to empty
//string - how many rans
public class Network{

	private ArrayList<Transmitter> stations;

	/**
	Create a network of transmitters.
	@param size The size of the network
	*/
	public Network(){

		this.stations = new ArrayList<Transmitter>();
	}

	public String toString(){

		return "A network of " + stations.size() + " transmitters.";
	}

	/**
	Add a transmitter to the network.
	@param t The transmitter to be added
	*/
	public void addStation(Transmitter t){

		stations.add(t);
	}

	/**
	How many transmitter are there?
	@return The number of transmitters
	*/
	public int size(){

		return stations.size();
	}

	/**
	Get a transmitter at an index
	@param index The index
	@return The transmitter at that index
	*/
	public Transmitter getStation(int index){

		return stations.get(index);
	}

	/**
	Return the signal strength at a location - this is the maximum 
	signal strength from the transmitter
	@param x The x coordinate of the location
	@param y The y coordinate of the location
	@return The maximum signal strength found
	*/
	public double getSignal(double x, double y){

		double strongest = 0.0;

		for(int i = 0; i < size(); i++){

			double signal = getStation(i).getSignal
		}
	}
}