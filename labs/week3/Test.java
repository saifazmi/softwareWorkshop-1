public class Test{
	
	public static void main(String[] args){
		
		Module sw = new Module("Software Workshop", "Jon Rowe");
		
		System.out.println();		
		System.out.println(sw);
		System.out.println("Module Name: " + sw.getName());
		
		sw.setLecturer("Mark Lee");
		System.out.println(sw);
		System.out.println("Lecturer's Name: " + sw.getLecturer());
		
		Student alf = new Student("Saif Azmi", 1367219);
		
		
		System.out.println();
		System.out.println(alf);
		
		System.out.println("Student's Name: " + alf.getName());
		System.out.println("Student ID: " + alf.getId());
		
		alf.setName("Saifullah Azmi");
		System.out.println(alf);
		
		alf.setModule(0, sw);
		alf.setModule(1, new Module("FOCS", "Dan Ghica"));
		alf.setModule(2, new Module("Lang & Logic", "Mark Lee"));
		
		System.out.println("Modules enrolled in - ");
		
		for(int i = 0; i < 3; i++){
			
			System.out.println(alf.getModule(i));
		}
	}
}
