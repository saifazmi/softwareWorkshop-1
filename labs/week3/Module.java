public class Module{
	
	private String name;
	private String lecturer; //attribute
	
	public Module(String name, String lecturer){
		
		this.name = name;
		this.lecturer = lecturer;
	}
	
	//prints the object
	public String toString(){
		
		return "Module: " + name + ", " + lecturer;
	}
	
	//get Methods
	
	public String getName(){
		
		return this.name;
	}
	
	public String getLecturer(){
		
		return this.lecturer;
	}
	
	//set Methods
	
	public void setLecturer(String lecturer){
	
		this.lecturer = lecturer;
	}
}
