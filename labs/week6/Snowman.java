/**
Construction of snowman graphics
*/
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.Graphics2D;
import java.awt.Color;

public class Snowman{
	
	private Ellipse2D.Double head;
	private Ellipse2D.Double body1;
	private Ellipse2D.Double body2;
	private Line2D.Double arm1;

	/**
	Create the snowman
	*/
	public Snowman(int xCoord, int yCoord, int headSize){

		head = new Ellipse2D.Double(xCoord, yCoord, headSize, headSize);
		body1 = new Ellipse2D.Double(xCoord - headSize, yCoord + headSize,
											2 * headSize, 2 * headSize);
		body2 = new Ellipse2D.Double(xCoord - headSize, yCoord + headSize,
											3 * headSize, 3 * headSize);
		arm1 = new Line2D.Double(xCoord - headSize, yCoord + 2.5 * headSize,
								xCoord - headSize, yCoord + 2.5 * headSize);
	}

	/**
	Draw the snowman on a graphics object
	*/
	public void draw(Graphics2D g){

		g.setColor(Color.WHITE);
		g.fill(head);
		g.fill(body1);
		g.fill(body2);

		g.setColor(Color.BLACK);
		g.draw(head);
		g.draw(body1);
		g.draw(body2);
		g.draw(arm1);
	}
}