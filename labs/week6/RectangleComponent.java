/**
A component to hold a rectangle
*/
import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Color;

public class RectangleComponent extends JComponent{
	
	private int xCoord;
	private int yCoord;

	/**
	Construct a component with specified coordinates for rectangle location
	@param xCoord the x coordinate of top left corner
	@param yCoord the y coordinate of top left corner
	*/
	public RectangleComponent(int xCoord, int yCoord){

		super();	// calling const. of class JComponent
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}

	/**
	Draw a rectangle
	@param g the graphics object we are drawing on
	*/
	public void paintComponent(Graphics g){

		Graphics2D g2 = (Graphics2D)g;

		Rectangle box = new Rectangle(xCoord, yCoord, 200, 100);

		//Color c = new Color(255, 0, 0);
		g2.setColor(Color.RED);
		//g2.draw(box);
		g2.fill(box);

		g2.setColor(Color.BLUE);
		g2.drawString("Here is a rectangle", 200, 100);
	}
}