/**
A component to hold a Snowman
*/
import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class SnowmanComponent extends JComponent{

	private Snowman frosty;

	/**
	Construct a component with specified coordinates for snowman location
	@param xCoord the x coordinate of top left corner
	@param yCoord the y coordinate of top left corner
	@param headSize the size of the snowman's head
	*/
	public SnowmanComponent(int xCoord, int yCoord, int headSize){

		super();
		
		frosty = new Snowman(xCoord, yCoord, headSize);
	}

	/**
	Draw a snowman
	@param g the graphics object we are drawing on
	*/
	public void paintComponent(Graphics g){

		Graphics2D g2 = (Graphics2D)g;

		frosty.draw(g2);
	}
}