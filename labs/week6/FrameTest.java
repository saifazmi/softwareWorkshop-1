import javax.swing.JFrame;	// for using JFrame

public class FrameTest{

	public static void main(String[] args){

		JFrame frame = new JFrame();

		frame.setSize(600, 600);
		frame.setTitle("My Snowman");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//RectangleComponent compR = new RectangleComponent(100, 100);
		//frame.add(compR);

		SnowmanComponent compS = new SnowmanComponent(275, 100, 50);
		frame.add(compS);

		frame.setVisible(true);
	}
}