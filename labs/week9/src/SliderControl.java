import java.util.Observable;
import java.util.Observer;
import javax.swing.JSlider;

/**
 * Created by saif on 26/11/14.
 */
public class SliderControl extends JSlider implements Observer{

    private SunflowerModel model;

    public SliderControl(SunflowerModel model, int min, int max, int value){

        super(min, max, value);
        this.model = model;
    }


    @Override
    public void update(Observable o, Object arg) {

        setValue((int)(model.getAngle() * 100));
    }
}
