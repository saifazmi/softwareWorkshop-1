package week8;

import java.util.Observable;
import java.util.Observer;
import javax.swing.JLabel;

public class CelciusView extends JLabel implements Observer{
	
	private TemperatureModel model;
	
	public CelciusView(TemperatureModel model){
		
		super();
		
		this.model = model;
		
		double value = model.getCelcius();
		setText(value + " Celcius");
	}
	
	@Override
	public void update(Observable obs, Object obj) {
		
		double value = model.getCelcius();
		setText(value + " Celcius");
	}
	
	
}
