package week8;

import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;

public class FahrenheitView extends JLabel implements Observer{
	
	private TemperatureModel model;
	
	public FahrenheitView(TemperatureModel model){
		
		super();
		
		this.model = model;
		
		double value = model.getFahrenheit();
		setText(value + " Fahrenhiet");
	}

	@Override
	public void update(Observable obs, Object obj) {
		
		DecimalFormat df = new DecimalFormat("0.0");
		
		double value = model.getFahrenheit();
		setText(df.format(value) + " Fahrenheit");
	}

}
