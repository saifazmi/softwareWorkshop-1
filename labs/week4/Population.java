/**
Represents a population of people and an infection that passes between them.
*/

import java.util.Random;
import java.util.ArrayList;

public class Population{
	
	private ArrayList<Person> population;
	private double infect;
	private double recover;
	private Random generator;
	
	/**
	Creates a population of people.
	@param popsize The population size
	@param initial The initially infected population
	@param infect The infection rate
	@param recover The recovery rate
	*/ 
	public Population(int popsize, double initial, double infect, double recover){
	
		this.population = new ArrayList<Person>();
		this.infect = infect;
		this.recover = recover;
		this.generator = new Random();
		
		for(int i = 0; i < popsize; i++){
			
			Person p = new Person();
			if(generator.nextDouble() < initial){
				p.setInfected(true);
			}
			
			population.add(p);
		}
	}
	
	public String toString(){
		
		return "Number of people = " + getPopSize();
	}
	
	/**
	Gets population size
	@return Population Size
	*/
	public int getPopSize(){
		
		return this.population.size();
	}
	
	/**
	Gets the infection rate
	@return The infection rate
	*/
	public double getInfect(){
		
		return this.infect;
	}
	
	/**
	Change the infection rate
	@param infect The new infection rate
	*/
	public void setInfect(double infect){
		
		this.infect = infect;
	}
	
	/**
	Is the person at a specific index infected?
	@param index The index of the person
	@return Whether or not the person is infected.
	*/
	public boolean isInfected(int index){
		
		return population.get(index).isInfected();
	}
	
	/**
	How many people are infected?
	@return The number of infected people.
	*/
	public int howManyInfected(){
				
		int count = 0;
		
		for(int i = 0; i < getPopSize(); i++){
			
			if(isInfected(i)){
				
				count++;
			}
		}
		
		return count;			
	}
	
	/**
	The proprotion of infected people
	@return Proportion of infected people.
	*/
	public double proportionInfected(){
		
		return (double)howManyInfected()/getPopSize();
	}

	/**
	Update the population
	*/
	public void update(){
		
		for(int i = 0; i < getPopSize(); i++){
			
			if(isInfected(i)){
				
				if(generator.nextDouble() < recover){
					
					population.get(i).setInfected(false);
				}
			}
			
			else{
				
				int j = generator.nextInt(getPopSize());
				
				if(isInfected(j) && generator.nextDouble() < infect)
					population.get(i).setInfected(true);
			}
		}
	}
}