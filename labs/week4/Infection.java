/**
Test the infection model
*/

public class Infection{
	
	public static void main(String[] args){
		
		Person p = new Person();
		System.out.println(p);
		
		p.setInfected(true);
		System.out.println(p);
		
		Population pop = new Population(100, 0.8, 0.25, 0.2);
		System.out.println(pop);
		System.out.println(pop.getInfect());
		
		System.out.println(pop.howManyInfected());
		System.out.println(pop.proportionInfected());
		
		for(int i = 0; i < 20; i++){
			
			pop.update();
			System.out.println(pop.proportionInfected());
		}
	}
}
