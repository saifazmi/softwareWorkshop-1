/**
Assess signal strength across grid.
*/
public class SignalStrength{

	private Network net;
	private double[][] signal;
	private int range;

	/**
	Store signal strength of network.
	@param net the netowrk of transmitters
	@param range the maximum vlaue of the location coordinates
	*/
	public SignalStrength(Network net, int range){

		this.net = net;
		this.range = range;
		signal = new double[range][range];

		for(int i = 0; i < range; i++){

			for(int j = 0; j < range; j++){

				signal[i][j] = net.getSignal(i,j);
			}
		}
	}

	public String toString(){

		return "Range of grid is " + range + " units";
	}

	/**
	Average signal strength
	@return the average signal strength
	*/
	public double averageSignal(){

		double count = 0.0;

		for(int i = 0; i < range; i++){

			for(int j = 0; j < range; j++){

				count += signal[i][j];
			}
		}

		return count/(range * range);
	}
}