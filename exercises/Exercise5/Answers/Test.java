/**
Test the network model
*/
import java.text.DecimalFormat;

public class Test{

	public static void main(String[] args){

		DecimalFormat df = new DecimalFormat("0.00");

		System.out.println("\nTesting Class: TRANSMITTER");
		System.out.println("---------------------------\n");

		// Creating a new transmitter "t1" using class Transmitter
		Transmitter t1 = new Transmitter(1.0,	// xCoord
										 0.5,	// yCoord 
										 80.0);	// power

		// Creating a new transmitter "t2" using class Transmitter
		Transmitter t2 = new Transmitter(8.0, 1.2, 80.0);
		// Creating a new transmitter "t3" using class Transmitter
		Transmitter t3 = new Transmitter(17.5, 17.0, 80.0);
		
		// Testing the toString() of class Transmitter
		System.out.println("Where is transmitter \"t1\" located?" + 
							"\n> " + t1);

		// Testing the getSignal() of class Transmitter
		System.out.println("\nSignal strength from transmitter \"t1\" at " + 
							"mobile location x = 2.3, y = 1.5" +
							"\n> " + df.format(t1.getSignal(2.3, 1.5)));

		System.out.println();
		//------------------------------------------------------------------------

		System.out.println("\nTesting Class: NETWORK");
		System.out.println("-----------------------\n");

		// Creating a new network "net" using class Network
		Network net = new Network();

		// Testing the toString() of class Network
		System.out.println("How many transmitters in the network?" + 
							"\n> " + net);

		// Testing the add() of class Network
		net.add(t1);
		System.out.println("\nAdded: " + t1);

		net.add(t2);
		System.out.println("Added: " + t2);
		
		net.add(t3);
		System.out.println("Added: " + t3 + "\n");

		// After transmitters/stations have been added to the network
		System.out.println("How many transmitters in the network now?"+
							"\n> " + net);

		// Testing the size() of class Network
		System.out.println("\nHow many stations in the network?" + 
							"\n> " + net.size());

		// Testing the getTransmitter() of class Network
		System.out.println("\nLocation of 3rd station in the network?" + 
							"\n> " + net.getTransmitter(2));

		// Testing the getSignal() of class Network
		System.out.println("\nBest signal at location x = 14.7, y = 15.3" + 
							"\n> " + df.format(net.getSignal(14.7, 15.3)));		

		System.out.println();
		//------------------------------------------------------------------------

		System.out.println("\nTesting Class: SIGNAL MAP");
		System.out.println("--------------------------\n");

		// Creating a new signal map "sm" using class SignalMap
		SignalMap sm = new SignalMap(4.9,	// threshold 
									 20, 	// size
									 net);	// network

		// Testing the toString() of class SignalMap
		System.out.println("How big is the map?" + 
							"\n> " + sm);

		// Testing the display() of class SignalMap
		System.out.println("\n-----------------------------------");
		System.out.println("Signal Map of the current network-");
		System.out.println("***********************************\n");

		sm.display();

		// Testing the poorSignal() of class SignalMap
		System.out.println("\nProportion of poor signal across the map?" + 
							"\n> " + df.format(sm.poorSignal()));

		/* Creating a new Network "net2" using class Network
		 * to test the compare() of class SignalMap
		 */
		System.out.println("\nCreated a new network2 for comparison...");
		Network net2 = new Network();

		System.out.println("\nHow many transmitters in the network2?" + 
							"\n> " + net2);

		// Creating three new Transmitters to add to "net2"
		Transmitter t4 = new Transmitter(3.5, 5.4, 70.0);
		Transmitter t5 = new Transmitter(8.6, 7.0, 90.0);
		Transmitter t6 = new Transmitter(16.3, 4.0, 100.0);

		net2.add(t4);
		System.out.println("\nAdded: " + t4);

		net2.add(t5);
		System.out.println("Added: " + t5);
		
		net2.add(t6);
		System.out.println("Added: " + t6 + "\n");

		// After transmitters/stations have been added to the network
		System.out.println("How many transmitters in the network2 now?"+
							"\n> " + net2);

		System.out.println("\n------------------------------------------------");
		System.out.println("Signal Map for the areas where the new network2	" +
							"\nhas stronger signal than previous netowrk");
		System.out.println("************************************************\n");

		// Calling the compare() of class SignalMap
		sm.compare(net2);

		System.out.println("\n-----------------------END-----------------------");
		System.out.println();
		//------------------------------------------------------------------------
	}
}