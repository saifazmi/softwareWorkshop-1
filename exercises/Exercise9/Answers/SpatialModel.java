import java.util.Observable;

/**
 * Model of spatial network class
 */
public class SpatialModel extends Observable {

    private SpatialNetwork spatNet;

    /**
     * Construct model of spatial network
     * @param spatNet the spatial network object
     */
    public SpatialModel(SpatialNetwork spatNet) {

        super();
        this.spatNet = spatNet;
    }

    /**
     * The X coordinate of the node
     * @param index the index of the node
     * @return x-coordinate of the node
     */
    public double getX(int index) {

        return spatNet.getX(index);
    }

    /**
     * The Y coordinate of the node
     * @param index the index of the node
     * @return y-coordinate of the node
     */
    public double getY(int index) {

        return spatNet.getY(index);
    }

    /**
     * The number of nodes in spatial network
     * @return number of nodes in the network
     */
    public int getNumNodes() {

        return spatNet.getNumNodes();
    }

    /**
     * The connection threshold of spatial network
     * @return connection threshold of spatial network
     */
    public double getThreshold() {

        return spatNet.getThreshold();
    }

    /**
     * Checks if the two nodes are connected depending upon the
     * threshold value set earlier
     * @param indexN1 index of node1
     * @param indexN2 index of node2
     * @return true if nodes are connected or false if they are not connected
     */
    public boolean connected(int indexN1, int indexN2) {

        return spatNet.connected(indexN1, indexN2);
    }

    /**
     * Change the Number of Nodes in spatial network
     * @param numNodes the new value of number of nodes
     */
    public void setNumNodes(int numNodes) {

        spatNet.setNumNodes(numNodes);
        setChanged();
        notifyObservers();
    }

    /**
     * Change the connection threshold for spatial network
     * @param threshold the new threshold value of spatial network
     */
    public void setThreshold(double threshold) {

        spatNet.setThreshold(threshold);
        setChanged();
        notifyObservers();
    }
}
