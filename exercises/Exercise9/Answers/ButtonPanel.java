import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel to hold the reset and exit buttons
 */
public class ButtonPanel extends JPanel {

    /**
     * Construct the button panel containing the reset and exit buttons
     * @param model the spatial model object
     * @param initNumNodes initial value of number of nodes on slider
     * @param initThreshold initial value of threshold on slider
     */
    public ButtonPanel(SpatialModel model,
                       int initNumNodes,
                       int initThreshold) {

        // create the reset button
        JButton reset = new JButton("Reset");

        /*
        Set the action of reset button to
        revert the value of threshold and number of nodes to
        their initial values and also reset the slider
         */
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                model.setNumNodes(initNumNodes);
                model.setThreshold(initThreshold / 100.0);
            }
        });

        // create the exit button
        JButton exit = new JButton("Exit");

        /*
        Set the action of exit button to
        exit the program when clicked
         */
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                System.exit(0);
            }
        });

        // add reset and exit button on to the panel
        add(reset);
        add(exit);
    }
}
