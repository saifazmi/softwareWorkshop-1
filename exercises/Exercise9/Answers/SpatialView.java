import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.Observable;
import java.util.Observer;

/**
 * Panel containing the spatial network
 */
public class SpatialView extends JPanel implements Observer {

    private SpatialModel model;

    /**
     * Construct the panel to hold the spatial network
     * @param model the spatial model object
     */
    public SpatialView(SpatialModel model) {

        super();
        this.model = model;
    }

    /**
     * Calls the UI delegate's paint method, if the UI delegate is non-null
     * @param g the Graphics object to protect
     */
    public void paintComponent(Graphics g) {

        Graphics2D g2 = (Graphics2D) g;

        int height = getHeight();
        int width = getWidth();
        g2.clearRect(0, 0, width, height);

        // area to draw graph
        double drawArea = Math.min(height, width);

        // radius of the nodes
        double radius = drawArea / 100.0;

        g2.setColor(Color.BLACK);

        /*
        Go through list one at a time
        for each one create a new Ellipse and draw it
         */
        for (int i = 0; i < model.getNumNodes(); i++) {

            double x = model.getX(i) * drawArea;
            double y = model.getY(i) * drawArea;

            Ellipse2D.Double n = new Ellipse2D.Double(x - radius, y - radius,
                    2 * radius, 2 * radius);

            g2.fill(n);
        }

        /*
        Go through every possible pair of nodes and check if they
        are connected and draw the connection
         */
        for (int i = 0; i < model.getNumNodes(); i++) {

            for (int j = 0; j < model.getNumNodes(); j++) {

                if (model.connected(i, j) && j != i) {

                    Line2D.Double connection =
                            new Line2D.Double(model.getX(i) * drawArea,    // x1
                                    model.getY(i) * drawArea,              // y1
                                    model.getX(j) * drawArea,              // x2
                                    model.getY(j) * drawArea               // y2
                            );

                    g2.draw(connection);
                }
            }
        }
    }

    /**
     * This method is called whenever the observed object is changed.
     * @param o the observable object
     * @param arg an argument passed to the notifyObservers method
     */
    @Override
    public void update(Observable o, Object arg) {

        repaint();
    }
}
