import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * Spatial Network of random nodes
 */
public class SpatialNetwork {

    private int numNodes;
    private double threshold;
    private ArrayList<Point2D.Double> nodes;

    /**
     * Construct a spatial network
     * @param numNodes number of nodes in the network
     * @param threshold network connection threshold
     */
    public SpatialNetwork(int numNodes, double threshold) {

        this.numNodes = numNodes;
        this.threshold = threshold;
        nodes = new ArrayList<Point2D.Double>();
        placeNodes();
    }

    /**
     * Creates an ArrayList of randomly generated node coordinates
     */
    private void placeNodes() {

        /*
        To make sure that every time the number of nodes is changed the
        whole graph is not redrawn randomly

        Rather a single (or multiple) nodes are added randomly and the
         previous nodes are preserved
         */

        if (numNodes > nodes.size()) {

            // number of new nodes to add
            int nodesToAdd = numNodes - nodes.size();

            for (int i = 0; i < nodesToAdd; i++){

                // randomly generate the node coordinates between 0.0 -> 1.0
                double x = Math.random();
                double y = Math.random();

                // add a new node to network  with the generated coordinates
                nodes.add(new Point2D.Double(x, y));
            }
        }

        /*
        To make sure that one or more nodes are removed from the existing
        ArrayList of nodes and the whole network is not re-drawn
         */
        else {

            for(int i = nodes.size()-1; i >= numNodes; i--){

                nodes.remove(i);
                nodes.trimToSize();
            }
        }
    }

    /**
     * The X coordinate of the node
     * @param index the index of the node
     * @return x-coordinate of the node
     */
    public double getX(int index) {

        return nodes.get(index).getX();
    }

    /**
     * The Y coordinate of the node
     * @param index the index of the node
     * @return y-coordinate of the node
     */
    public double getY(int index) {

        return nodes.get(index).getY();
    }

    /**
     * The number of nodes in spatial network
     * @return number of nodes in the network
     */
    public int getNumNodes() {

        return this.numNodes;
    }

    /**
     * The connection threshold of spatial network
     * @return connection threshold of spatial network
     */
    public double getThreshold() {

        return this.threshold;
    }

    /**
     * Checks if the two nodes are connected depending upon the
     * threshold value set earlier
     * @param indexN1 index of node1
     * @param indexN2 index of node2
     * @return true if nodes are connected or false if they are not connected
     */
    public boolean connected(int indexN1, int indexN2) {

        // making sure no garbage values are used
        double distance = 0.0;

        // formula for calculating the distance between two coordinates
        distance = Math.sqrt(
                Math.pow((getX(indexN2) - getX(indexN1)), 2) +
                        Math.pow((getY(indexN2) - getY(indexN1)), 2)
        );

        if (distance < threshold) {

            return true;
        } else {

            return false;
        }
    }

    /**
     * Change the Number of Nodes in spatial network
     * @param numNodes the new value of number of nodes
     */
    public void setNumNodes(int numNodes) {

        this.numNodes = numNodes;
        placeNodes();
    }

    /**
     * Change the connection threshold for spatial network
     * @param threshold the new threshold value of spatial network
     */
    public void setThreshold(double threshold) {

        this.threshold = threshold;
    }
}
