import javax.swing.JPanel;
import java.awt.BorderLayout;

/**
 * Holds all the components of spatial network viewer
 */
public class SpatialComponent extends JPanel {

    /**
     * Construct all the components of the spatial network viewer
     * @param spatNet the spatial network object
     * @param min minimum value of the slider
     * @param max maximum value of the slider
     * @param initNumNodes initial value of number of nodes on the slider
     * @param initThreshold initial value of threshold on the slider
     */
    public SpatialComponent(SpatialNetwork spatNet,
                            int min,
                            int max,
                            int initNumNodes,
                            int initThreshold) {

        // set layout to BorderLayout
        super(new BorderLayout());

        // create model
        SpatialModel model = new SpatialModel(spatNet);

        // create views
        SpatialView view = new SpatialView(model);

        // create controls panels
        NumberPanel numberPanel = new NumberPanel(model, min, 2 * max, initNumNodes);
        ThresholdPanel thresholdPanel = new ThresholdPanel(model, min, max, initThreshold);
        ButtonPanel buttons = new ButtonPanel(model, initNumNodes, initThreshold);

        // make views observer model
        model.addObserver(view);
        model.addObserver(numberPanel);
        model.addObserver(thresholdPanel);

        // add different components to the frame
        add(numberPanel, BorderLayout.NORTH);
        add(view, BorderLayout.CENTER);
        add(buttons, BorderLayout.EAST);
        add(thresholdPanel, BorderLayout.SOUTH);
    }
}
