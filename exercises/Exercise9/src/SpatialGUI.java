import javax.swing.JFrame;

/**
 * Spatial Network's GUI
 */
public class SpatialGUI {

    public static void main(String[] args) {

        int initNumNodes = 130;
        double initThreshold = 0.21;
        int frameSize = 600;

        // create spatial network object
        SpatialNetwork spatNet = new SpatialNetwork(initNumNodes, initThreshold);

        // create spatial component object
        SpatialComponent spatComp = new SpatialComponent(
                spatNet,                    // spatial network object
                0,                          // minimum slider value
                100,                        // maximum slider value
                initNumNodes,               // initial num of nodes
                (int)(initThreshold * 100.0)// initial threshold
        );

        //create JFrame object
        JFrame frame = new JFrame("Spatial Network Viewer");

        // set frame size and default close action
        frame.setSize(frameSize, frameSize);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // add spatial component on to the frame
        frame.add(spatComp);

        // make the frame visible
        frame.setVisible(true);
    }
}
