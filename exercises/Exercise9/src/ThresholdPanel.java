import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

/**
 * Panel to hold the threshold slider and label
 */
public class ThresholdPanel extends JPanel implements Observer {

    private final JLabel label = new JLabel("Threshold");
    private JSlider slider;
    private SpatialModel model;

    /**
     * Construct the threshold panel containing the slider and label
     * @param model the spatial model object
     * @param min minimum value of the slider
     * @param max maximum value of the slider
     * @param initThreshold initial value of threshold on the slider
     */
    public ThresholdPanel(SpatialModel model,
                          int min,
                          int max,
                          int initThreshold) {

        // set layout to BorderLayout
        super(new BorderLayout());

        this.model = model;
        this.slider = new JSlider(min, max, initThreshold);

        // adding ticks to the slider
        slider.setPaintTicks(true);
        slider.setMajorTickSpacing((max - min) / 10);

        // adding tick values to the slider
        slider.setPaintLabels(true);
        slider.setLabelTable(slider.createStandardLabels((max - min) / 10));

        // inner class to listen to slider value changes
        slider.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {

                int value = slider.getValue();
                model.setThreshold(value / 100.0);

            }
        });

        // add label and slider on to the panel
        add(label, BorderLayout.WEST);
        add(slider);
    }

    /**
     * This method is called whenever the observed object is changed.
     * @param o the observable object
     * @param arg an argument passed to the notifyObservers method
     */
    @Override
    public void update(Observable o, Object arg) {

        slider.setValue((int) (Math.round(model.getThreshold() * 100.0)));
    }
}
