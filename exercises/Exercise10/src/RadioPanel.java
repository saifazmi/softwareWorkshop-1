import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 * Holds the radio buttons to set the Difficulty level of Mine Sweeper Game
 */
public class RadioPanel extends JPanel {

    /**
     * Construct a Radio Panel
     * @param model the Mine Model object
     */
    public RadioPanel(MineModel model) {

        super();

        // the easy difficulty button
        JRadioButton easy = new JRadioButton("Easy");
        easy.addActionListener(e -> model.newGame(MineSweeper.Difficulty.EASY));

        // the medium difficulty button
        JRadioButton medium = new JRadioButton("Medium", true);
        medium.addActionListener(e -> model.newGame(MineSweeper.Difficulty.MEDIUM));

        // the hard difficulty button
        JRadioButton hard = new JRadioButton("Hard");
        hard.addActionListener(e -> model.newGame(MineSweeper.Difficulty.HARD));

        /*
        add them to a button group, so that
        one button can be used at a time
         */
        ButtonGroup difficultyLevel = new ButtonGroup();
        difficultyLevel.add(easy);
        difficultyLevel.add(medium);
        difficultyLevel.add(hard);

        // add the buttons to the panel
        add(easy);
        add(medium);
        add(hard);
    }
}
