import java.util.Arrays;
import java.util.Random;

/**
 * MineSweeper holds the state of the game
 */
public class MineSweeper {

    /**
     * Difficulty Level that can be used.
     * It controls the number of mines
     */
    public static enum Difficulty {

        /**
         * Easy difficulty level (10 mines)
         */
        EASY,

        /**
         * Medium difficulty level (15 mines)
         */
        MEDIUM,

        /**
         * Hard difficulty level (20 mines)
         */
        HARD
    }

    /**
     * Represents the state of every square on the board
     */
    public static enum SquareState {

        /**
         * Square is a Mine
         */
        MINE,

        /**
         * Square is a Detonated Mine
         */
        DETONATED_MINE,

        /**
         * Square is Revealed
         */
        REVEALED,

        /**
         * Square is Unrevealed
         */
        UNREVEALED
    }

    // number of rows
    public static final int rows = 10;

    // number of columns
    public static final int columns = 10;

    private Difficulty diffLevel;           // current difficulty level
    private int numMines;                   // current number of mines
    private SquareState[][] board;          // board
    private int numUnRevealed;              // number of unrevealed squares
    private SquareState currentBoardState;  // state of board revealed or unrevealed
    private boolean gameOver;               // to store if game is over

    /**
     * Construct the board with certain number of mines
     *
     * @param diffLevel the difficulty level of the game
     */
    public MineSweeper(Difficulty diffLevel) {

        this.diffLevel = diffLevel;
        this.board = new SquareState[rows][columns];
        this.gameOver = false;
        this.numUnRevealed = rows * columns;
        this.currentBoardState = SquareState.UNREVEALED;
        setNumMines();
        placeMines();
        setBoard();
    }

    /**
     * Sets the board to its initial state
     */
    private void setBoard() {

        for (int i = 0; i < rows; i++) {

            for (int j = 0; j < columns; j++) {

                // to avoid over-writing an existing mine
                if (board[i][j] != SquareState.MINE) {

                    board[i][j] = this.currentBoardState;
                }
            }
        }
    }

    /**
     * Sets the number of mines according to the difficulty level
     */
    private void setNumMines() {

        switch (this.diffLevel) {

            case EASY:
                this.numMines = 10;
                break;

            case MEDIUM:
                this.numMines = 15;
                break;

            case HARD:
                this.numMines = 20;
                break;

            default:
                System.err.print("< Invalid difficulty level! >");
                break;
        }
    }

    /**
     * Places randomly generated mines on the board
     */
    private void placeMines() {

        Random randMines = new Random();

        int row = randMines.nextInt(rows);
        int col = randMines.nextInt(columns);

        for (int i = 0; i < numMines; i++) {

            // to avoid over-writing an existing mine
            while (getSquareState(row, col) == SquareState.MINE) {

                row = randMines.nextInt(rows);
                col = randMines.nextInt(columns);
            }

            board[row][col] = SquareState.MINE;
        }
    }

    /**
     * Checks if the coordinates are present on board
     * to avoid IndexOutOfBounds Error while checking
     * for adjacent mines
     *
     * @param row the row coordinate of the square
     * @param col the column coordinate of the square
     * @return true if the square is on board and false if it is out of the board
     */
    private boolean isOnBoard(int row, int col) {

        return !((row < 0 || col < 0 ||
                row >= board.length || col >= board[0].length));
    }

    /**
     * Change the state of a square
     *
     * @param row   the row coordinate of the square
     * @param col   the column coordinate of the square
     * @param state the new state of the square
     */
    public void setSquareState(int row, int col, SquareState state) {

        this.board[row][col] = state;
    }

    /**
     * The state of a square
     *
     * @param row the row coordinate of the square
     * @param col the column coordinate of the square
     * @return the current state of the board
     */
    public SquareState getSquareState(int row, int col) {

        return board[row][col];
    }

    /**
     * The number of mines adjacent to the corresponding square
     *
     * @param row the row coordinate of the square
     * @param col the column coordinate of the square
     * @return the number of mines adjacent to the corresponding square
     */
    public int getAdjacentMines(int row, int col) {

        int count = 0;

        for (int i = (row - 1); i <= (row + 1); i++) {

            for (int j = (col - 1); j <= (col + 1); j++) {

                // coordinate should be on board and a mine or detonated mine
                if (isOnBoard(i, j) && !(i == row && j == col)) {

                    if (getSquareState(i, j) == SquareState.MINE ||
                            getSquareState(i, j) == SquareState.DETONATED_MINE) {
                        count++;
                    }
                }
            }
        }

        return count;
    }

    /**
     * The difficulty level of the game
     *
     * @return the current difficulty level of the game
     */
    public Difficulty getDiffLevel() {

        return this.diffLevel;
    }

    /**
     * Whether or not the game is over
     *
     * @return true if the game is over and false if  not
     */
    public boolean getGameOver() {

        return this.gameOver;
    }

    /**
     * Sets the game over state
     *
     * @param gameState the game over state
     */
    public void setGameOver(boolean gameState) {

        this.gameOver = gameState;
    }

    /**
     * Decreases the number of unrevealed squares by 1
     */
    public void setNumUnRevealed() {

        this.numUnRevealed--;
    }

    /**
     * Has the player won?
     *
     * @return true if the player has won the game and false if not
     */
    public boolean hasWon() {

        return numUnRevealed == numMines;
    }

    /**
     * Creates a new game
     *
     * @param diffLevel the difficulty level of the new game
     */
    public void newGame(Difficulty diffLevel) {

        this.diffLevel = diffLevel;
        this.gameOver = false;
        this.currentBoardState = SquareState.UNREVEALED;
        this.numUnRevealed = rows * columns;

        for (int i = 0; i < board.length; i++) {

            Arrays.fill(board[i], null);
        }

        setNumMines();
        placeMines();
        setBoard();
    }

    /**
     * Change the current state of board
     *
     * @param currentBoardState the new state of board
     */
    public void setCurrentBoardState(SquareState currentBoardState) {

        this.currentBoardState = currentBoardState;
        setBoard();
    }

    /**
     * The state of the board
     *
     * @return the current state of the board
     */
    public SquareState getCurrentBoardState() {

        return this.currentBoardState;
    }

    public void expand(int row, int col) {

        for (int i = (row - 1); i <= (row + 1); i++) {

            for (int j = (col - 1); j <= (col + 1); j++) {

                if (isOnBoard(i, j) &&
                        !(i == row && j == col) &&
                        getSquareState(i, j) != SquareState.MINE) {

                    setSquareState(i, j, SquareState.REVEALED);
                }
            }
        }
    }
}