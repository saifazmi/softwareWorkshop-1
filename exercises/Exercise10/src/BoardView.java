import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

/**
 * Holds the board for Mine Sweeper Game
 */
public class BoardView extends JPanel implements Observer {

    private MineModel model;
    private JButton[][] square;
    private int rows = MineSweeper.rows;
    private int columns = MineSweeper.columns;

    /**
     * Construct a new mine sweeper board
     *
     * @param model the Mine Model object
     */
    public BoardView(MineModel model) {

        super();

        this.model = model;
        this.square = new JButton[rows][columns];

        // set the layout to Grid
        setLayout(new GridLayout(rows, columns));

        // create the board
        for (int i = 0; i < rows; i++) {

            for (int j = 0; j < columns; j++) {

                square[i][j] = new JButton(" ");
                square[i][j].addActionListener(new SquareListener(model, i, j));
                square[i][j].setBackground(Color.DARK_GRAY);
                square[i][j].setFocusable(false);
                add(square[i][j]);
            }
        }
    }

    /**
     * This method is called whenever the observed object is changed.
     *
     * @param o   the observable object
     * @param arg an argument passed to the notifyObservers method
     */
    @Override
    public void update(Observable o, Object arg) {

        // for each square on board:
        for (int i = 0; i < rows; i++) {

            for (int j = 0; j < columns; j++) {

                MineSweeper.SquareState squareState = model.getSquareState(i, j);
                MineSweeper.SquareState boardState = model.getCurrentBoardState();
                int adjacentMines = model.getAdjacentMines(i, j);

                if (squareState == MineSweeper.SquareState.UNREVEALED ||
                        squareState == MineSweeper.SquareState.MINE &&
                                !model.getGameOver() ||
                        model.hasWon() &&
                                boardState == MineSweeper.SquareState.UNREVEALED) {

                    square[i][j].setText(" ");
                    square[i][j].setBackground(Color.DARK_GRAY);
                    square[i][j].setEnabled(true);
                }

                if (squareState == MineSweeper.SquareState.DETONATED_MINE) {

                    square[i][j].setText("X");
                    square[i][j].setBackground(Color.RED);
                    square[i][j].setEnabled(false);
                } else if (squareState == MineSweeper.SquareState.REVEALED
                        && boardState == MineSweeper.SquareState.UNREVEALED) {

                    if (adjacentMines > 0) {

                        square[i][j].setText(String.valueOf(adjacentMines));
                    } else {

                        square[i][j].setText(" ");
                    }

                    square[i][j].setBackground(Color.WHITE);
                    square[i][j].setEnabled(false);
                }

                if (model.getGameOver() || boardState == MineSweeper.SquareState.REVEALED) {

                    if (squareState == MineSweeper.SquareState.MINE) {
                        square[i][j].setBackground(Color.YELLOW);
                        square[i][j].setText("X");
                    } else if (squareState == MineSweeper.SquareState.DETONATED_MINE) {

                        square[i][j].setBackground(Color.RED);
                        square[i][j].setText("X");
                    } else if (squareState == MineSweeper.SquareState.UNREVEALED) {

                        square[i][j].setBackground(Color.WHITE);
                        square[i][j].setText(" ");
                    }

                    square[i][j].setEnabled(false);
                }

                if (model.hasWon() &&
                        squareState == MineSweeper.SquareState.MINE) {

                    square[i][j].setText("X");
                    square[i][j].setForeground(Color.WHITE);
                    square[i][j].setEnabled(false);
                }
            }
        }
        repaint();
    }
}
