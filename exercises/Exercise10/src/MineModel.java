import java.util.Observable;

/**
 * Model of Mine Sweeper class
 */
public class MineModel extends Observable {

    private MineSweeper mSweep;

    /**
     * Construct a new model of Mine Sweeper class
     *
     * @param mSweep the mine sweeper object
     */
    public MineModel(MineSweeper mSweep) {

        super();
        this.mSweep = mSweep;
    }

    /**
     * Change the state of a square
     *
     * @param row   the row coordinate of the square
     * @param col   the column coordinate of the square
     * @param state the new state of the square
     */
    public void setSquareState(int row, int col, MineSweeper.SquareState state) {

        mSweep.setSquareState(row, col, state);
        setChanged();
        notifyObservers();
    }

    /**
     * The state of a square
     *
     * @param row the row coordinate of the square
     * @param col the column coordinate of the square
     * @return the current state of the board
     */
    public MineSweeper.SquareState getSquareState(int row, int col) {

        return mSweep.getSquareState(row, col);
    }

    /**
     * The number of mines adjacent to the corresponding square
     *
     * @param row the row coordinate of the square
     * @param col the column coordinate of the square
     * @return the number of mines adjacent to the corresponding square
     */
    public int getAdjacentMines(int row, int col) {

        return mSweep.getAdjacentMines(row, col);
    }

    /**
     * The difficulty level of the game
     *
     * @return the current difficulty level of the game
     */
    public MineSweeper.Difficulty getDiffLevel() {

        return mSweep.getDiffLevel();
    }

    /**
     * Whether or not the game is over
     *
     * @return true if the game is over and false if  not
     */
    public boolean getGameOver() {

        return mSweep.getGameOver();
    }

    /**
     * Sets the game over state
     *
     * @param gameState the game over state
     */
    public void setGameOver(boolean gameState) {

        mSweep.setGameOver(gameState);
        setChanged();
        notifyObservers();
    }

    /**
     * Decreases the number of unrevealed squares by 1
     */
    public void setNumUnRevealed() {

        mSweep.setNumUnRevealed();
        setChanged();
        notifyObservers();
    }

    /**
     * Has the player won?
     *
     * @return true if the player has won the game and false if not
     */
    public boolean hasWon() {

        return mSweep.hasWon();
    }

    /**
     * Creates a new game
     *
     * @param diffLevel the difficulty level of the new game
     */
    public void newGame(MineSweeper.Difficulty diffLevel) {

        mSweep.newGame(diffLevel);
        setChanged();
        notifyObservers();
    }

    /**
     * Change the current state of board
     *
     * @param currentBoardState the new state of board
     */
    public void setCurrentBoardState(MineSweeper.SquareState currentBoardState) {

        mSweep.setCurrentBoardState(currentBoardState);
        setChanged();
        notifyObservers();
    }

    /**
     * The state of the board
     *
     * @return the current state of the board
     */
    public MineSweeper.SquareState getCurrentBoardState() {

        return mSweep.getCurrentBoardState();
    }

    public void expand(int row, int col) {

        mSweep.expand(row, col);
    }
}
