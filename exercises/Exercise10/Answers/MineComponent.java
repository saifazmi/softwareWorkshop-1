import javax.swing.JPanel;
import java.awt.BorderLayout;

/**
 * Holds all the component of the Mine Sweeper Game
 */
public class MineComponent extends JPanel {

    /**
     * Construct a new component
     * @param mSweep the Mine Sweeper object
     */
    public MineComponent(MineSweeper mSweep) {

        // set layout to Border
        super(new BorderLayout());

        // create the model
        MineModel model = new MineModel(mSweep);

        // create the board
        BoardView board = new BoardView(model);

        // create the control panel
        ControlPanel cp = new ControlPanel(model);

        // make board Observer the model
        model.addObserver(board);

        // add the board and control panel to the panel
        add(board, BorderLayout.CENTER);
        add(cp, BorderLayout.SOUTH);

    }
}
