import javax.swing.JPanel;
import java.awt.BorderLayout;

/**
 * Holds the controls of the game
 */
public class ControlPanel extends JPanel {

    /**
     * Construct the control panel
     * @param model the Mine Model object
     */
    public ControlPanel(MineModel model) {

        // set the layout to Border
        super(new BorderLayout());

        /*
        Create the radio button controls to
        set the difficulty of the game
         */
        RadioPanel radioButtons = new RadioPanel(model);

        /*
        Create the JButton controls to
        control the mine sweeper board
         */
        ButtonPanel controlButtons = new ButtonPanel(model);

        // add them on to the panel
        add(radioButtons, BorderLayout.WEST);
        add(controlButtons, BorderLayout.EAST);
    }

}
