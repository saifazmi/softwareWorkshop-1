import javax.swing.JFrame;

/**
 * The GUI for Mine Sweeper Game
 */
public class MineSweeperGUI {

    public static void main(String[] args) {

        // Construct a Mine Sweeper Object
        MineSweeper mSweep = new MineSweeper(MineSweeper.Difficulty.MEDIUM);

        // Construct the component holder
        MineComponent comp = new MineComponent(mSweep);

        // Construct a frame to put all the components
        JFrame frame = new JFrame("Mine Sweeper");
        frame.setSize(800, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // add component to frame
        frame.add(comp);

        // set the frame to visible
        frame.setVisible(true);
    }
}
