import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The listener for every board button
 */
public class SquareListener implements ActionListener {

    private MineModel model;
    private int row;
    private int col;

    /**
     * Construct a new square listener
     * @param model the Mine Model object
     * @param row the row coordinate of the square
     * @param col the column coordinate of the square
     */
    public SquareListener(MineModel model, int row, int col) {

        this.model = model;
        this.row = row;
        this.col = col;
    }

    /**
     * Invoked when an action occurs.
     * @param e the ActionEvent object
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        // if a mine
        if (model.getSquareState(row, col) == MineSweeper.SquareState.MINE) {

            // then detonate the mine
            model.setSquareState(row, col, MineSweeper.SquareState.DETONATED_MINE);
            // game over -> true
            model.setGameOver(true);
        } else {

            // other wise just reveal the panel
                model.setSquareState(row, col, MineSweeper.SquareState.REVEALED);
        }

        // decrease the number of unrevealed square
        model.setNumUnRevealed();
    }
}
