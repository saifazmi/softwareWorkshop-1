import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Holds the buttons to control the board of Mine Sweeper Game
 */
public class ButtonPanel extends JPanel {

    /**
     * Construct a ButtonPanel
     * @param model the Mine Model object
     */
    public ButtonPanel(MineModel model) {

        super();

        /* the restart button
           starts a new game
         */
        JButton restart = new JButton("Restart");
        restart.addActionListener(e -> model.newGame(model.getDiffLevel()));

        /* the reveal button
           reveals all the mines on board
         */
        JButton reveal = new JButton("Reveal");
        reveal.addActionListener(e -> model.setCurrentBoardState(MineSweeper.SquareState.REVEALED));

        /*
           the exit button
           exits the game
         */
        JButton exit = new JButton("Exit");
        exit.addActionListener(e -> System.exit(0));

        // add the buttons to the panel
        add(restart);
        add(reveal);
        add(exit);
    }
}
