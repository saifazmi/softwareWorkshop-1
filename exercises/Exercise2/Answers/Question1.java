/* Program to printout the longest string
 * from a list of user_inputed strings
 */

import java.util.Scanner;
import java.util.ArrayList;

public class Question1{
	
	public static void main(String[] args){
		
		Scanner in = new Scanner(System.in);
		
		ArrayList<String> list = new ArrayList<String>();
				
		System.out.println("\nEnter a string (enter \"stop\" to finish)");
		System.out.print("> ");  // just to make input look elegant
		String s = in.nextLine();
		
		while(!s.equals("stop")){
			
			// to prevent empty strings to be added in list
			if(!s.isEmpty()){
				list.add(s);
			}
			
			System.out.println("Enter a string (enter \"stop\" to finish)");
			System.out.print("> ");
			s = in.nextLine();
		}
		
		/* This part of code runs only when the list is
		 * not empty
		 */
		 
		if(!list.isEmpty()){
			
			String longest = list.get(0); // initializing longest  variable for comparison
		
			// loop to go through each element of the list once
			for(int i = 0; i < list.size(); i++){
			
				/* Checking to see if the current string is 
				 * longer than the longest string we started
				 * with.
				 *
				 * This process is repeated with every element
				 * of the list.
				 */
				 
				if(list.get(i).length() > longest.length()){
					
					// if the check is true, current element becomes longest
					longest = list.get(i);
				}
			}
			
			/* code to print a line to seprate output
			 * from all the input
			 */
			
			for(int i = 1; i <= 40; i++){
				System.out.print("-");
			}
			
			System.out.println("\nThe longest string was " + "\"" + longest + "\"");
		}
		
		else{
			
			// if the list is empty, this message is printed
			System.out.println("\nThe list was Empty !!!");
		}
		
		/* just some end code to let the user know when 
		 * the program has terminated
		 */
		 
		for(int i = 1; i <= 37; i++){
				System.out.print("-");
				if(i == 18){
					System.out.print("END");
				}
			}
		System.out.println("\n");
	}
}
