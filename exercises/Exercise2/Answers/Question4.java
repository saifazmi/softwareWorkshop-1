/* Program to create a new ArrayList which contains
 * each element from the list only once, thus preventing any
 * duplicate elements in the newList
 */
 
import java.util.Scanner;
import java.util.ArrayList;

public class Question4{

	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		
		ArrayList<String> list = new ArrayList<String>();
		
		System.out.println("\nEnter a string (enter \"stop\" to finish)");
		System.out.print("> ");  // just to make input look elegant
		String s = in.nextLine();
		
		while(!s.equals("stop")){
			
			// to prevent empty strings to be added in list
			if(!s.isEmpty()){
				list.add(s);
			}
		
			System.out.println("Enter a string (enter \"stop\" to finish)");
			System.out.print("> ");
			s = in.nextLine();
		}
		
		/* This part of code runs only when the list is
		 * not empty
		 */
		 
		if(!list.isEmpty()){
		
			ArrayList<String> newList = new ArrayList<String>();
			
			// initializing the newList with the first element of list
			newList.add(list.get(0)); 
			
			for(int i = 0; i < list.size(); i++){
				
				/* Logic applied - 
				 *
				 * using contains() to see if the newList already contains the current
				 * element from list.
				 *
				 * if it does have that element then it's not added to the newList,
				 * hence preventing any duplicate copies of any element being copied
				 * to the newList.
				 */
				 	
				if(!newList.contains(list.get(i))){
						
					newList.add(list.get(i));
				}
			}
			
			/* code to print a line to seprate output
			 * from all the input
			 */
					
			for(int i = 1; i <= 40; i++){
				
				System.out.print("-");
			}
			
			System.out.println("\nPrinting the list after removing \nduplicate entries -\n");
			
			for(int i = 0; i < newList.size(); i++){
			
				System.out.println(newList.get(i));		
			}
		}
		
		else{
			
			// if the list is empty, this message is printed
			System.out.println("\nThe list was Empty !!!");
		}
		
		/* just some end code to let the user know when 
		 * the program has terminated
		 */
		 
		for(int i = 1; i <= 37; i++){
		
			System.out.print("-");
			if(i == 18){
				System.out.print("END");
			}
		}
				
		System.out.println("\n");
	}
}
