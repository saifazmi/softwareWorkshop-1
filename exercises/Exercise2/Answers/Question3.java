/* Program to replace string elements having more than 
 * three characters in the list with just first three
 * characters of the string element.
 */

import java.util.Scanner;
import java.util.ArrayList;

public class Question3{

	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		
		ArrayList<String> list = new ArrayList<String>();
		
		System.out.println("\nEnter a string (enter \"stop\" to finish)");
		System.out.print("> ");  // just to make input look elegant
		String s = in.nextLine();
		
		while(!s.equals("stop")){
			
			// to prevent empty strings to be added in list
			if(!s.isEmpty()){
				list.add(s);
			}
		
			System.out.println("Enter a string (enter \"stop\" to finish)");
			System.out.print("> ");
			s = in.nextLine();
		}
		
		/* This part of code runs only when the list is
		 * not empty
		 */
		 
		if(!list.isEmpty()){
			
			for(int i = 0; i < list.size(); i++){
				
				if(list.get(i).length() > 3){
					
					String first_three = "";
					
					/* using substring() to extract the first three
					 * characters of the string.
					 *
					 * And then adding them back to the list using set()
					 */
					 
					first_three = list.get(i).substring(0,3);
					
					list.set(i,first_three);
				}
			}
			
			/* code to print a line to seprate output
			 * from all the input
			 */
			 
			for(int i = 1; i <= 40; i++){
				
				System.out.print("-");
			}
			
			System.out.println("\nPrinting the list after reducing it to \nfirst three characters of origional string -\n");
			
			for(int i = 0; i < list.size(); i++){
			
				System.out.println(list.get(i));		
			}
		}
		
		else{
			
			// if the list is empty, this message is printed
			System.out.println("\nThe list was Empty !!!");
		}
		
		/* just some end code to let the user know when 
		 * the program has terminated
		 */
		 
		
		for(int i = 1; i <= 37; i++){
		
			System.out.print("-");
			if(i == 18){
				System.out.print("END");
			}
		}
				
		System.out.println("\n");
	}
}
