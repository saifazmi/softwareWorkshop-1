/* Program to reverse the order of elements in the
 * list and print out the list to confirm it
 */

import java.util.Scanner;
import java.util.ArrayList;

public class Question2{
	
	public static void main(String[] args){
		
		Scanner in = new Scanner(System.in);
		
		ArrayList<String> list = new ArrayList<String>();
		
		System.out.println("\nEnter a string (enter \"stop\" to finish)");
		System.out.print("> ");  // just to make input look elegant
		String s = in.nextLine();
		
		while(!s.equals("stop")){
			
			// to prevent empty strings to be added in list
			if(!s.isEmpty()){
				list.add(s);
			}
			
			System.out.println("Enter a string (enter \"stop\" to finish)");
			System.out.print("> ");
			s = in.nextLine();
		}
		
		/* This part of code runs only when the list is
		 * not empty
		 */
		 
		if(!list.isEmpty()){
			
			/* Logic applied in reversing -
			 
			 * first 		-> 	last
			 			<-
			 			
			 * first - 1 	->	last + 1
			 			<-
			 			
			 * first - 2	-> 	last + 2
			 			<-
			 			
			 * first - n	->	last - n ... and so on
			 			<-
			 
			 * Using (start < end) makes sure that the
			 * middle element is not swaped by itself.
			 
			 * The same has been applied using a for loop
			 */
			 
			for(int start = 0, end = list.size() - 1; start < end; start++, end--){
			
				// a temporary variable to store the element being over-written
				String temp = list.set(start, list.get(end));
				
				/* adding the temporarily deleted element back 
				 * to the list in the proper position
				 */
				 
				list.set(end,temp);
			}
			
			/* code to print a line to seprate output
			 * from all the input
			 */
			 
			for(int i = 1; i <= 40; i++){
				
				System.out.print("-");
			}
			
			System.out.println("\nPrinting the list of strings after \nreversing them -\n");
			
			for(int i = 0; i < list.size(); i++){
			
				System.out.println(list.get(i));	
			}
		}
		
		else{
			
			// if the list is empty, this message is printed
			System.out.println("\nThe list was Empty !!!");
		}
		
		/* just some end code to let the user know when 
		 * the program has terminated
		 */
		 
		for(int i = 1; i <= 37; i++){
		
			System.out.print("-");
			if(i == 18){
				System.out.print("END");
			}
		}
				
		System.out.println("\n");
	}
}
