/**
Test the infection model
*/

import java.text.DecimalFormat;

public class Infection{
	
	public static void main(String[] args){
		
		System.out.println("\nTesting class: Person -");
		System.out.println("-----------------------");
		
		// creting a Person "p" by using class Person
		Person p = new Person();
		// testing the toString() of class Person
		System.out.println("\ntoString() output - \n> " + p); // well person
		
		// testing the setInfected() of class Person
		p.setInfected(true);
		// status of Person p changes from well -> sick
		System.out.println("After using setInfected() - \n> " + p);  // sick person

		// testing the isInfected() of class Person
		System.out.println("Is the person infected? \n> " + p.isInfected()); // true

		System.out.println();
		//------------------------------------------------------------------

		System.out.println("\nTesting class: Population -");
		System.out.println("---------------------------");
		
		// for infected proportion trailing decimal points
		DecimalFormat df = new DecimalFormat("0.00");	
	/* creating a Population "pop1" by using class Population.
		 * Here I'm calling the first constructor without death rate
		 */
		Population pop1 = new Population(100,	// population size 
										0.1, 	// initially infected pop1.
										0.85, 	// infection rate
										0.2);	// recovery rate
		// testing the toString() of class Population
		System.out.println("\ntoString() output - \n> " + pop1); // Size of population = 100

		// testing the getPopSize() of class Population
		System.out.println("Population Size? \n> " + pop1.getPopSize());	// 100

		// testing the setInfect() of class Population
		pop1.setInfect(0.73);	// 0.85 -> 0.73
		// testing getInfect() of class Population
		System.out.println("Infection Rate? \n> " + pop1.getInfect()); // 0.73

		// testing the setRecover() of class Population
		pop1.setRecover(0.36);	// 0.20 -> 0.36
		// testing the getRecover() of class Population
		System.out.println("Recovery Rate? \n> " + pop1.getRecover()); // 0.36

		// testing the isInfected() of class Population
		System.out.println("Is Person 4 infected? \n> " + pop1.isInfected(3));	// true/false, anyone

		// testing the howManyInfected() of class Population
		System.out.println("Initially infected people? \n> " + pop1.howManyInfected()); // about 10
		
		// testing the proportionInfected() of class Population
		System.out.println("Proportion of Infected population? \n> " + df.format(pop1.proportionInfected())); // *depends*

		System.out.println();
		//------------------------------------------------------------------

		// Running a simulation for 10 days (less days for testing)
		System.out.println("SIMUlATION: 1");
		System.out.println(">>>>>>>>>>>>>>");

		for(int i = 0; i < 10; i++){

			// testing the update() of class Population
			System.out.println("\n[DAY: " + (i+1) + "]");
			pop1.update();
			System.out.println("Proportion of Infected \t= " + df.format(pop1.proportionInfected()));
			System.out.println("Survivors \t\t= " + pop1.getPopSize() + " people");
		}

		System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		System.out.println();
		//------------------------------------------------------------------
		
		System.out.println("Testing class: Population (with Death Rate) -");
		System.out.println("---------------------------------------------");
		
		/* creating a Population "pop2" by using class Population.
		 * Here I'm calling the second constructor with death rate
		 */
		Population pop2 = new Population(100,	// population size 
										0.1, 	// initially infected pop1.
										0.73, 	// infection rate
										0.36,	// recovery rate
										0.10);	// death rate

		// testing the setDeathRate() of class Population
		pop2.setDeathRate(0.15);	// 0.10 -> 0.15
		// testing the getDeathRate() of class Population
		System.out.println("\nDeath rate? \n> " + pop2.getDeathRate());
		System.out.println("\n*Rest of the parameters are the same*");

		// Running a simulation for 10 days (less days for testing)
		System.out.println("\nSIMUlATION: 2");
		System.out.println(">>>>>>>>>>>>>>");

		for(int i = 0; i < 10; i++){

			// testing the update() of class Population
			System.out.println("\n[DAY: " + (i+1) + "]");
			pop2.update();
			System.out.println("Proportion of Infected \t= " + df.format(pop2.proportionInfected()));
			System.out.println("Infected \t\t= " + pop2.howManyInfected());
			System.out.println("Survivors \t\t= " + pop2.getPopSize() + " people");
		}

		System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		System.out.println();
	}
}