public class Student{

	private String name;
	private int id;
	private Module[] modules;
	
	public Student(String name, int id){
		
		this.name = name;
		this.id = id;
		this.modules = new Module[3];
	}
	
	// prints the object
	public String toString(){
		
		return this.name + ", " + this.id;
	}
	
	//ANSWER: 2
	// checking if the student has the module passed in
	public boolean onModule(Module module){
		
		boolean found = false;
		
		for(int i = 0; i < modules.length; i++){
		
			if(modules[i].equals(module)){
				
				found = true;
			}	
		}
		
		return found;
	}
	
	// get methods
	public String getName(){
	
		return this.name;
	}
	
	public int getId(){
		
		return this.id;
	}
	
	public Module getModule(int index){
		
		return this.modules[index];
	}
	
	// set methods
	public void setName(String name){
		
		this.name = name;
	}
	
	public void setModule(int index, Module module){
		
		this.modules[index] = module;	
	}	
}
