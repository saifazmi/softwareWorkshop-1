public class Test{

	public static void main(String[] args){
		
		// Created extra objects for testing different functions
		// creating a new instancce of class Module
		Module sw = new Module("Software Workshop", "Jon Rowe");
		// creating a new instancce of class Module
		Module focs = new Module("FOCS", "Dan Ghica");
		// creating a new instancce of class Module
		Module lnl = new Module("Lang. & Logic", "Mark Lee");
		// creating a new instancce of class Module
		Module bom = new Module("Biz. Org. & Mngt.", "Chris Collinge");
		
		System.out.println();
		
		// testing toString() from class Module
		System.out.println(sw);
		
		// testing getName() from class Module
		System.out.println("Module Name: " + sw.getName());
		
		// testing getLecturer from class Module
		System.out.println("Lecturer's Name: " + sw.getLecturer());
		
		// testing setLecturer() from class Module
		sw.setLecturer("Martin Escardo");
		System.out.println(sw);
		System.out.println();
		
		/* ANSWER: 1 
		 * testing equals() of class Module
		 */
		 
		System.out.println("Comparing Modules using equals() -");
		
		// condition 1 (false)
		if(sw.equals(focs)){
			
			System.out.println("Same Module");
		}
		
		else{
			
			System.out.println("Different Modules");
		}
		
		// condition 2 (true)
		if(sw.equals(sw)){
			
			System.out.println("Same Module");
		}
		
		else{
			
			System.out.println("Different Modules");
		}
		
		System.out.println();
		
		// ----------------------------------------------------------------
		
		// creating a new instance of class Student
		Student alf = new Student("Saif Azmi", 1367219);
		
		// testing toString() of calss Student
		System.out.println(alf);
		
		// testing getName() of class Student
		System.out.println("Student's Name: " + alf.getName());
		
		// testing getId() of class Student
		System.out.println("Student's ID: " + alf.getId());
		
		// testing setName() of class Student
		alf.setName("Saifullah Azmi");
		System.out.println(alf);
		
		// testing setModule() of class Student
		// since i have careated objects for modules I'm replacing them here
		alf.setModule(0, sw);
		//alf.setModule(1, new Module("FOCS", "Dan Ghica"));
		//alf.setModule(2, new Module("Lang. & Logic", "Mark Lee"));
		alf.setModule(1, focs);
		alf.setModule(2, lnl);
		
		// testing getModule() of class Student
		System.out.println("\nModules enrolled in -");
		
		for(int i = 0; i < 3; i++){
			
			System.out.println(alf.getModule(i));
		}
		
		System.out.println();
		
		/* ANSWER: 2
		 * testing onModule() of class Student
		 */
		
		System.out.println("Checking if a student is enrolled on a Module using onModule() -"); 
		// condition 1 (false)
		if(alf.onModule(bom)){
			
			System.out.println("Enrolled in " + bom.getName());
		}
		
		else{
			
			System.out.println("NOT Enrolled in " + bom.getName());
		}
		
		// condition 2 (true)
		if(alf.onModule(focs)){
			
			System.out.println("Enrolled in " + focs.getName());
		}
		
		else{
			
			System.out.println("NOT Enrolled in " + focs.getName());
		}
		
		System.out.println();
		
		// ----------------------------------------------------------------
		
		// ANSWER: 3
		
		//Ans: 3(a)
		// creating a new instance of class TutorGroup
		TutorGroup tg = new TutorGroup("Ayush Singh");
		
		// Ans: 3(c)
		// initialising before hand to check toString()
		// testing addStudent() from class TutorGroup
		tg.addStudent("Saifullah Azmi");
		tg.addStudent("Nur Abdul Shakir");
		tg.addStudent("Dominic Abel");
		tg.addStudent("Thahleel Abid");
		tg.addStudent("Aydin Ahmadli");
		
		// Ans: 3(b)
		// testing toString() from class TutorGroup
		System.out.println(tg);
		
		// testing getTutor() from class TutorGroup
		System.out.println("Tutor's Name: " + tg.getTutor());
		
		// testing setTutor() from class TutorGroup
		tg.setTutor("Ayush Joshi");
		System.out.println(tg);
		
		/*
		 * commented out because ArrayList is already initialised	
		// Ans: 3(c)
		// testing addStudent() from class TutorGroup
		tg.addStudent("Saifullah Azmi");
		tg.addStudent("Nur Abdul Shakir");
		tg.addStudent("Dominic Abel");
		tg.addStudent("Thahleel Abid");
		tg.addStudent("Aydin Ahmadli");
		*/
		
		// testing getStudents() from class TutorGroup
		System.out.println("\nStudents part of Tutorial Group - ");
		
		/* the following way works and we get a list of students
		 * but it's not elegent
		 */
		//System.out.println(tg.getStudents());
		
		// Another way to print out the list of students "elegently" is -
		
		for(int i = 0; i < tg.getStudents().size(); i++){
			
			System.out.println(tg.getStudents().get(i));
		}		
		
		
		// ----------------------------------------------------------------
		System.out.println();
	}
}
