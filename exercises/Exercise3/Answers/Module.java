public class Module{
	
	private String name;
	private String lecturer;
	
	public Module(String name, String lecturer){
		
		this.name = name;
		this.lecturer = lecturer;
	}
	
	// prints the object
	public String toString(){
		
		return "Module: " + this.name + ", " + this.lecturer;
	}
	
	// ANSWER: 1
	// object comparison
	public boolean equals(Module module){
		
		return this.name == module.getName();
	}
	
	// get Methods
	public String getName(){
		
		return this.name;
	}
	
	public String getLecturer(){
		
		return this.lecturer;
	}
	
	// set Methods
	public void setLecturer(String lecturer){
		
		this.lecturer = lecturer;
	}
}
