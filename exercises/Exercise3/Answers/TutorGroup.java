// ANSWER: 3

import java.util.ArrayList;

// Ans: 3(a)
public class TutorGroup{

	private String tutor;
	private ArrayList<String> students;
	
	public TutorGroup(String tutor){
		
		this.tutor = tutor;
		this.students = new ArrayList<String>();
	}
	
	// Ans: 3(b)
	// prints the object
	public String toString(){
		
		return "Tutor: " + this.tutor + "\nStudents - \n" + students;
	}
	
	// get methods
	public String getTutor(){
		
		return this.tutor;
	}
	
	// set methods
	public void setTutor(String tutor){
		
		this.tutor = tutor;
	}
	
	// Ans: 3(c)
	// add's new student to the tutor group
	public void addStudent(String student){
		// write condition to check existing students
		this.students.add(student);
	}
	
	// return's the list of students in the tutor group
	public ArrayList<String> getStudents(){
	
		return students;
	}
}

