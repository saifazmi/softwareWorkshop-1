/**
To test the HouseComponent with House class and StreetComponent with House class
*/
import javax.swing.JFrame;
//import java.awt.Dimension;

public class HouseTest{

	public static void main(String[] args) {
		
		JFrame frame1 = new JFrame();

		frame1.setSize(600, 600);
		//frame1.setMinimumSize(new Dimension(600, 600));
		frame1.setTitle("Mansion");

		/* DISPOSE_ON_CLOSE
		 * to make sure that closing one window doesn't
		 * exit the whole program
		 */
		frame1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		

		HouseComponent mansion = new HouseComponent(150, 250, 300);
		frame1.add(mansion);

		frame1.setVisible(true);
		
		JFrame frame2 = new JFrame();

		frame2.setSize(1000, 600);
		//frame2.setMinimumSize(new Dimension(1000, 600));
		frame2.setTitle("Neighbourhood");
		frame2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		StreetComponent neighbourhood = new StreetComponent(50, 250, 150, 5);
		frame2.add(neighbourhood);

		frame2.setVisible(true);
	}
}