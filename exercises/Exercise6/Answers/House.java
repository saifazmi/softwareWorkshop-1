/**
Construction of house graphics
*/
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.geom.Line2D;
import java.awt.geom.Ellipse2D;
import java.awt.Graphics2D;
import java.awt.Color;

public class House{

	private Rectangle2D.Double building;
	private Line2D.Double roofL;
	private Line2D.Double roofR;
	private Rectangle2D.Double door;

	// challenges
	private RoundRectangle2D.Double window;
	private Line2D.Double winH;
	private Line2D.Double winV;
	
	private Line2D.Double chimV1;
	private Line2D.Double chimV2;
	private RoundRectangle2D.Double chimH;

	private Ellipse2D.Double doorKnob;

	/**
	Create the different parts of the house
	@param xCoord x coordinate of top-left corner of the wall of the house
	@param yCoord y coordinate of top-left corner of the wall of the house
	@param buildSize width of the house
	*/
	public House(int xCoord, int yCoord, int buildSize){

		building = new Rectangle2D.Double(	xCoord, 
											yCoord, 
											buildSize, 
											buildSize);	
		
		roofL = new Line2D.Double(	xCoord, 
									yCoord, 
									xCoord + buildSize * 0.5, 
									yCoord - buildSize * 0.5);
		
		roofR = new Line2D.Double(	xCoord + buildSize, 
									yCoord,
									xCoord + buildSize * 0.5,
									yCoord - buildSize * 0.5);

		door = new Rectangle2D.Double(	xCoord + buildSize * 0.5, 
										yCoord + buildSize * 0.375,
										buildSize * 0.325, 
										buildSize * 0.625);

		window = new RoundRectangle2D.Double(	xCoord + buildSize * 0.125,
												yCoord + buildSize * 0.125,
												buildSize * 0.25,
												buildSize * 0.25,
												10, 10);
		
		winV = new Line2D.Double(	xCoord + buildSize * 0.25,
									yCoord + buildSize * 0.125,
									xCoord + buildSize * 0.25,
									yCoord + buildSize * 0.375);

		winH = new Line2D.Double(	xCoord + buildSize * 0.125,
									yCoord + buildSize * 0.25,
									xCoord + buildSize * 0.375,
									yCoord + buildSize * 0.25);

		doorKnob = new Ellipse2D.Double(	xCoord + buildSize * 0.5625,
											yCoord + buildSize * 0.6875,
											buildSize * 0.0625,
											buildSize * 0.0625);

		chimV1 = new Line2D.Double(	xCoord + buildSize * 0.75,
									yCoord - buildSize * 0.25,
									xCoord + buildSize * 0.75,
									yCoord - buildSize * 0.375);

		chimV2 = new Line2D.Double(	xCoord + buildSize * 0.875,
									yCoord - buildSize * 0.125,
									xCoord + buildSize * 0.875,
									yCoord - buildSize * 0.375);

		chimH = new RoundRectangle2D.Double(xCoord + buildSize * 0.6875,
											yCoord - buildSize * 0.4375,
											buildSize * 0.25,
											buildSize * 0.0625,
											5, 5);
	}

	/**
	Draw the house on a graphics object
	@param g the graphics object
	*/
	public void draw(Graphics2D g){

		g.setColor(Color.BLACK);
		g.draw(building);
		g.draw(roofL);
		g.draw(roofR);
		g.draw(door);

		g.draw(window);
		g.draw(winV);
		g.draw(winH);

		g.draw(doorKnob);

		g.draw(chimV1);
		g.draw(chimV2);
		g.draw(chimH);
	}
}