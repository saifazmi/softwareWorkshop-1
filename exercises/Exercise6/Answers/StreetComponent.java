/**
A component to hold a street with house(s)
*/
import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class StreetComponent extends JComponent{

	private House[] villas;
	private int xCoord;
	private int yCoord;
	private int buildSize;
	private int numHouse;

	/**
	Construct a component with specified coordinates for the begining of street location
	@param xCoord x coordinate of the top-left corner of the wall of the first house
	@param yCoord y coordinate of the top-left corner of the wall of the first house
	@param buildSize width of the house(s) on the street
	@param numHouse number of houses on the street
	*/
	public StreetComponent(int xCoord, int yCoord, int buildSize, int numHouse){

		super();
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.buildSize = buildSize;
		this.numHouse = numHouse;
		this.villas = new House[numHouse];

		for(int i = 0; i < numHouse; i++){

			villas[i] = new House(	xCoord + (buildSize * i) + (buildSize/4 * i),
									yCoord,
									buildSize);
		}
	}

	/**
	Draw a street with houses
	@param g the graphics object
	*/
	public void paintComponent(Graphics g){

		Graphics2D g2 = (Graphics2D)g;

		for(int i = 0; i < numHouse; i++){

			villas[i].draw(g2);
			g2.drawString(	String.valueOf(i + 1), 
							(float)(xCoord + buildSize * 0.625 + buildSize * 0.25 * i + (buildSize * i)),
							(float)(yCoord + buildSize * 0.5625));
		}
	}
}