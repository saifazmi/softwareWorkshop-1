/**
A component to hold a house
*/
import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class HouseComponent extends JComponent{

	private House mansion;

	/**
	Construct a component with specified coordinates for house location
	@param xCoord x coordinate of the top-left corner of the wall of the house
	@param yCoord y coordinate of the top-left corner of the wall of the house
	@param buildSize width of the house
	*/
	public HouseComponent(int xCoord, int yCoord, int buildSize){

		super();

		mansion = new House(xCoord, yCoord, buildSize);
	}

	/**
	Draw a house
	@param g the graphics object
	*/
	public void paintComponent(Graphics g){

		Graphics2D g2 = (Graphics2D)g;

		mansion.draw(g2);
	}
}