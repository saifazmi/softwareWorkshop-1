package ex7;

/**
 * Component to hold a Infection History Graph
 */

import javax.swing.JComponent;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

public class VirusComponent extends JComponent {

    private double[] propInfected;
    private int frameSize; //i'm not using this anymore as I'm scaling my graph after resizing

    /**
     Construct a component with data to draw the graph with
     @param propInfected proportion of infected people in the population
     @param frameSize size of the JFrame
     */
    public VirusComponent(double[] propInfected, int frameSize) {

        super();

        this.propInfected = propInfected;
        this.frameSize = frameSize;
    }

    /**
     Draw a graph with proportion of infected people on every time step
     @param g the graphics object
     */
    public void paintComponent(Graphics g) {

        Graphics2D g2 = (Graphics2D) g;

        g2.setColor(Color.BLACK);

        Line2D.Double yAxis = new Line2D.Double(getWidth() * 0.15,  // x1
                getHeight() * 0.15, // y1
                getWidth() * 0.15,  // x2
                getHeight() * 0.85);    // y2
        g2.draw(yAxis);
        g2.drawString("Time", (float) (getWidth() * 0.5), (float) (getHeight() * 0.925));

        Line2D.Double xAxis = new Line2D.Double(getWidth() * 0.15, getHeight() * 0.85,
                getWidth() * 0.85, getHeight() * 0.85);
        g2.draw(xAxis);
        g2.drawString("Infections", (float) (getWidth() * 0.1125), (float) (getHeight() * 0.13));

        for (int i = 0; i < propInfected.length; i++) {

            Rectangle2D.Double infected = new Rectangle2D.Double(
                    i * (getWidth() * 0.70 / propInfected.length) + getWidth() * 0.15,
                    (getHeight() * 0.70) - (propInfected[i] * getHeight() * 0.70) + getHeight() * 0.15,
                    1, 1);

            //only using fill was not giving a readable graph
            g2.fill(infected);
            //so I drew a rectangle around it
            g2.draw(infected);
        }
    }
}