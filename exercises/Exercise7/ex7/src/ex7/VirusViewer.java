package ex7;

/**
Class to test the infection graph
*/
import javax.swing.JFrame;

public class VirusViewer{
	
	public static void main(String[] args) {
		
		int frameSize = 700;
		int days = 1000;
		double[] propInfected = new double[days];

		JFrame frame = new JFrame("Virus infection histroy");

		frame.setSize(frameSize, frameSize);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Creating a population
		Population pop = new Population(1000,	// pop size
										0.74,	// initially infected
										0.60,	// infection rate
										0.45,	// recovery rate
										0.00);	// death rate



		for(int i = 0; i < days; i++){ 

			propInfected[i] = pop.proportionInfected();

			pop.update();
		}

		VirusComponent infectionGraph = new VirusComponent(propInfected, frameSize);
		frame.add(infectionGraph);

		frame.setVisible(true);
	}	
}