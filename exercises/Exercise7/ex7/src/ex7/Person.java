package ex7;

/**
Describes a person and whether or not they are infected with a disease.
*/

public class Person{
	
	private boolean infected;
	
	/**
	Creates an uninfected person.
	*/
	public Person(){
		
		this.infected = false;
	}
	
	public String toString(){
		
		if(this.infected){
			
			return "Sick Person";
		}
		
		else{
			
			return "Well Person";	
		}
	}
	
	/**
	Tells us if a person is infected.
	@return Infection status.
	*/
	public boolean isInfected(){
		
		return infected;
	}
	
	/**
	Changes the infection status.
	@param infected New infection status.
	*/	
	public void setInfected(boolean infected){
		
		this.infected = infected;
	}
}
