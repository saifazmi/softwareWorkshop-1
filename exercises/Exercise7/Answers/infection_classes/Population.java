/**
Represents a population of people and an infection that passes between them.
*/

import java.util.Random;
import java.util.ArrayList;

public class Population{
	
	public ArrayList<Person> population;
	public ArrayList<Person> deadPeople;	
	private double infect;	// infection rate of the disease
	private double recover;	// recovery rate for the disease
	private double deathRate;	// random number generator
	private Random generator;
	
	/**
	Create a population, some of whom may be infected.
	@param popsize the population size
	@param initial the initially infected population
	@param infect the infection rate
	@param recover the recovery rate
	*/ 
	public Population(int popsize, double initial, double infect, double recover){
	
		this.infect = infect;
		this.recover = recover;
		this.deathRate = 0.0;
		this.generator = new Random();
		this.deadPeople = new ArrayList<Person>();
		this.population = new ArrayList<Person>();

		for(int i = 0; i < popsize; i++){
			
			Person p = new Person();
			if(generator.nextDouble() < initial){
				p.setInfected(true);
			}
			
			population.add(p);
		}
	}
	
	/**
	Create a population, some of whom may be infected.
	@param popsize the population size
	@param initial the initially infected population
	@param infect the infection rate
	@param recover the recovery rate
	@param deathRate the death rate
	*/ 
	public Population(int popsize, double initial, double infect, double recover, double deathRate){
	
		this.infect = infect;	
		this.recover = recover;
		this.deathRate = deathRate;
		this.generator = new Random();
		this.deadPeople = new ArrayList<Person>();
		this.population = new ArrayList<Person>();

		for(int i = 0; i < popsize; i++){
			
			Person p = new Person();
			if(generator.nextDouble() < initial){
				p.setInfected(true);
			}
			
			population.add(p);
		}
	}

	public String toString(){
		
		if(this.deathRate > 0.0){

			return "Size of population = " + getPopSize() + " people \n"
					+ "Dead People = " + deadPeople.size();
		}

		else{

			return "Size of population = " + getPopSize();
		}
	}
	
	/**
	Get population size
	@return the population Size
	*/
	public int getPopSize(){
		
		return this.population.size();
	}
	
	/**
	Get infection rate
	@return the infection rate
	*/
	public double getInfect(){
		
		return this.infect;
	}
	
	/**
	Get recovery rate
	@return the recovery rate
	*/
	public double getRecover(){

		return this.recover;
	}

	/**
	Get death rate
	@return the death rate
	*/
	public double getDeathRate(){

		return this.deathRate;
	}

	/**
	Change infection rate
	@param infect new infection rate
	*/
	public void setInfect(double infect){
		
		this.infect = infect;
	}
	
	/**
	Change recovery rate
	@param recover new recovery rate
	*/
	public void setRecover(double recover){

		this.recover = recover;
	}

	/**
	Change death rate
	@param deathRate new death rate
	*/
	public void setDeathRate(double deathRate){

		this.deathRate = deathRate;
	}

	/**
	Is the person at a specific index infected?
	@param index index of the person concerned
	@return Whether or not this person is infected
	*/
	public boolean isInfected(int index){
		
		return population.get(index).isInfected();
	}
	
	/**
	How many people are infected?
	@return the number of infected people in the population
	*/
	public int howManyInfected(){
				
		int count = 0;
		
		for(int i = 0; i < getPopSize(); i++){
			
			if(isInfected(i)){
				
				count++;
			}
		}
		
		return count;			
	}
	
	/**
	What proportion of the population is infected?
	@return the proportion of infected people in the population.
	*/
	public double proportionInfected(){
		
		return (double)howManyInfected()/getPopSize();
	}

	/**
	Update the population on the basis of Recovery, Infection and Death Rate set earlier.
	*/
	public void update(){
		
		// clearing the dead people list from the previous call of update()
		deadPeople.clear();

		for(int i = 0; i < getPopSize(); i++){
			
			// checks if the person[i] is infected
			if(isInfected(i)){
				
				// determines the probability of recovery for person[i]
				if(generator.nextDouble() < recover){
					
					// changes person[i] status from sick -> well
					population.get(i).setInfected(false);
				}

				// determines the probability of death for person[i]
				else if(generator.nextDouble() < deathRate){

					// adds the person[i] to the list of dead people
					deadPeople.add(population.get(i));
				}
			}
			
			else{
				
				// selects a random person[j] from the population list
				int j = generator.nextInt(getPopSize());
				
				/* determines the probability of this well person[i] to get
				 * infected by coming in contact of infected person[j]
				 */
				if(isInfected(j) && generator.nextDouble() < infect)
					
					// changes person[i] infection status from well -> sick
					population.get(i).setInfected(true);
			}
		}

		//  checking if there are any dead people in this iteration
		if(!deadPeople.isEmpty()){

			//  deleting dead people from the population list
			population.removeAll(deadPeople);

			// adjusts the size of ArrayList after removing dead people
			population.trimToSize();
		}
	}
}