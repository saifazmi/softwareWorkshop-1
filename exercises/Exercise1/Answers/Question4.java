/* Program to print a user inputed string in reverse
 * and check if that string is a palindrome or not
 */
 
 import java.util.Scanner;
 
 public class Question4{
 	
 	public static void main(String[] args){
 		
 		Scanner input = new Scanner(System.in);
 		
 		String reverse = "";
 		
 		System.out.print("Please input a string: ");
 		String userWord = input.nextLine();
 		
 		System.out.print(userWord + " backwards is ");
 		
 		for(int i = (userWord.length()-1); i >= 0; i--){
 			reverse = reverse + userWord.charAt(i);
 		}
 		
 		System.out.println(reverse);
 		
 		/* Step to make sure that the strings are compared on the basis of
 		 * their content and not Upper or Lower case for being a Palindrome.
 		 *
 		 * The reason I haven't done this before and avoided converting
 		 * both the strings instead of just one is because I wanted the 
 		 * reserve output to be identical to user input.
 		 */
 		 
 		reverse = reverse.toLowerCase();
 		userWord = userWord.toLowerCase();
 		
 		if(reverse.equals(userWord)){
 			System.out.println("It is a palindrome");
 		}
 		
 		else{
 			System.out.println("It is not a palindrome");
 		}
 	}
 }
