/* Program to print the characters of
 * a given string in alphabetical order
 */
 
 import java.util.Scanner;
 
 public class Question6{
 	
 	public static void main(String[] args){
 		
 		Scanner input = new Scanner(System.in);
 		
 		System.out.print("Please input a string: ");
 		String userWord = input.nextLine();
 		
 		userWord = userWord.replaceAll("\\p{Blank}","");
 		userWord = userWord.toLowerCase();
 		
 		while(userWord.length() > 0){
 		 	
	 		char smallest = userWord.charAt(0);
	 					
		 	for(int i = 0; i < userWord.length(); i++){
		 			
		 		if(userWord.charAt(i) < smallest){
		 			smallest = userWord.charAt(i);
		 		}
		 	}
		 		
		 	int charToDelete = userWord.indexOf(smallest);
		 	userWord = userWord.substring(0,charToDelete) + userWord.substring(charToDelete+1);
		 		
		 	System.out.println(smallest);
		 }
 	}
 }
