/*
 * Comment this program
 */
 
import java.util.Scanner;

public class Question3{
	
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		
		int n = 0;
		
		do{
			System.out.print("Please input a number: ");
			n = input.nextInt(); input.nextLine();
		
			if(n > 0){
				for(int i = n; i > 0; i--){
					for(int j = 0; j < i; j++){
						System.out.print("*");
					}
					
					System.out.println();
				}
			}
		
			else{
				System.out.println("Please input a positive number... \n");
			}
			
		} while(n <= 0);
	}
}
