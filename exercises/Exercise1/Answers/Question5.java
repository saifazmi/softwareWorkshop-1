/* Program to find the character which comes 
 * first alphabetically in a given string
 */
 
 import java.util.Scanner;
 
 public class Question5{
 	
 	public static void main(String[] args){
 		
 		Scanner input = new Scanner(System.in);
 		
 		System.out.print("Please input a string: ");
 		String userWord = input.nextLine();
 		
 		userWord = userWord.replaceAll("\\p{Blank}",""); //comment required
 		userWord = userWord.toLowerCase();
 		
 		System.out.println(userWord);
 		
 		char smallest = userWord.charAt(0);
 		
 		for(int i = 0; i < userWord.length(); i++){
 			if(userWord.charAt(i) < smallest){
 				smallest = userWord.charAt(i);
 			}
 		}
 		
 		System.out.println("The first character alphabetically is " + smallest);
 	}
 }
 
