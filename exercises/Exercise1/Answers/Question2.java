/*Program to print the smallest positive integer before a
 *negative integer is encountered in the input stream */

//need to complete this

import java.util.Scanner;

public class Question2{

	public static void main(String[] args){
        
		Scanner input = new Scanner(System.in);
        
		System.out.print("Please input a number: ");
		int num = input.nextInt(); input.nextLine();
        	
		int smallest = num;
        
		while(num >= 0){
		
	        	System.out.print("Please input a number: ");
	        	num = input.nextInt(); input.nextLine();
	        	
	        	if(num < smallest && num >= 0){
	        		smallest = num;
        		}	
        	}
        	
        	if(smallest >= 0){
			System.out.println("The smallest number was " + smallest);
		}
	}
}
