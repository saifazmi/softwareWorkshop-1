/* Program to print all the even numbers
 * between 0 and user_inputed number */

import java.util.Scanner;

public class Question1{
    
    public static void main(String[] args){
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Please input a number: ");
        int upperLimit = input.nextInt(); input.nextLine();
        
        for(int i=0; i <= upperLimit; i++){
            if((i%2) == 0){
                System.out.println(i);
            }
        }
    }
}
