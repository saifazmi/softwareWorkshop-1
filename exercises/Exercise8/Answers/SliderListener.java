/**
Slider input control
*/
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SliderListener implements ChangeListener{
	
	private EasterModel model;
	private JSlider slider;
	
	/**
	Construct a slider listener
	@param model the easter model
	@param slider the JSlider object
	*/
	public SliderListener(EasterModel model, JSlider slider){
		
		this.model = model;
		this.slider = slider;
	}

	@Override
	/**
	To catch the change in the state of slider and change the year accordingly
	*/
	public void stateChanged(ChangeEvent e) {
		
		int value = slider.getValue();
		model.setYear(value);
	}
}
