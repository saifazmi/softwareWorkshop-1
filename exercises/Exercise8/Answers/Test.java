/**
Test class for Easter class
*/

public class Test {

	public static void main(String[] args) {
		
		// Creating a new easter object
		Easter easter = new Easter(2001);

		// testing the get() methods of Easter class
		System.out.println(	"\nIn the year " + easter.getYear() + " Easter will be on " + 
							"\nDay: " + easter.getDay() + 
							"\nMonth: " + easter.getMonth());
		
		// testing the setYear() of Easter class
		easter.setYear(2015);
		System.out.println(	"\nIn the year " + easter.getYear() + " Easter will be on " + 
							"\nDay: " + easter.getDay() + 
							"\nMonth: " + easter.getMonth());
	}

}
