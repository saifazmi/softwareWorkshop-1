/**
Set the output to be displayed
*/
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;

public class EasterView extends JLabel implements Observer {
	
	private EasterModel model;
	private int sysYear;
	private int sysMonth;
	private int sysDay;
	
	/**
	Contruct an Easter View
	@param model the easter model for which view needs to be set
	*/
	public EasterView(EasterModel model){
		
		super();
		this.model = model;
		this.sysYear = Calendar.getInstance().get(Calendar.YEAR);
		this.sysMonth = Calendar.getInstance().get(Calendar.MONTH);
		this.sysDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

		int dayValue = model.getDay();
		String monthValue = new DateFormatSymbols().getMonths()[model.getMonth()-1];
		int yearValue = model.getYear();
		
		// proper grammer for output

		if((yearValue < sysYear) ||
			(yearValue == sysYear && model.getMonth() < sysMonth) ||
			(yearValue == sysYear && model.getMonth() == sysMonth && dayValue < sysDay)) {

			setText("In the year " + yearValue + 
					" Easter was on " + dayValue + " " + monthValue);
		}

		else{

			setText("In the year " + yearValue + 
					" Easter will be on " + dayValue + " " + monthValue);
		}
	}

	@Override
	/**
	Update the output when slider is moved
	*/
	public void update(Observable o, Object arg) {
		
		int dayValue = model.getDay();
		String monthValue = new DateFormatSymbols().getMonths()[model.getMonth()-1];
		int yearValue = model.getYear();
		
		if((yearValue < sysYear) ||
			(yearValue == sysYear && model.getMonth() < sysMonth) ||
			(yearValue == sysYear && model.getMonth() == sysMonth && dayValue > sysDay)) {

			setText("In the year " + yearValue + 
					" Easter was on " + dayValue + " " + monthValue);
		}

		else{

			setText("In the year " + yearValue + 
					" Easter will be on " + dayValue + " " + monthValue);
		}
	}
}
