/**
Model of Easter class
*/

import java.util.Observable;

public class EasterModel extends Observable{
	
	private Easter easter;
	
	/**
	Construct the easter model
	@param easter the Easter object
	*/
	public EasterModel(Easter easter){
		
		super();
		this.easter = easter;
	}
	
	/**
	Get the easter day for the corresponding year
	@return day the day on which easter falls
	*/
	public int getDay(){
		
		return easter.getDay();
	}
	
	/**
	Get the easter month for the corresponding year
	@return month the month on which easter falls
	*/
	public int getMonth(){
		
		return easter.getMonth();
	}

	/**
	Get the year for which easter day is calculated
	@return year the year for easter calculation
	*/
	public int getYear(){
		
		return easter.getYear();
	}
	
	/**
	Change the year for easter calculation and notify observers
	@param year the year for which easter day is to be calculated
	*/
	public void setYear(int year){
		
		easter.setYear(year);
		setChanged();
		notifyObservers();
	}
}
