/**
Calculating Easter day
*/

public class Easter {
	
	private int day;
	private int month;
	private int year;
	
	/**
	Construct Easter with the given year
	@param year the year for which easter day is to be calculated
	*/
	public Easter(int year){
		
		this.year = year;
		calcEaster(year);
	}
	
	/**
	The day for easter day for the corresponding year
	@return day the day on which easter falls
	*/
	public int getDay(){
		
		return day;
	}
	
	/**
	The month for easter day for the corresponding year
	@return month the month on which easter falls
	*/
	public int getMonth(){
		
		return month;
	}
	
	/**
	The year for which easter day is calculated
	@return year the year for easter calculations
	*/
	public int getYear(){
		
		return year;
	}
	
	/**
	Change the year for easter calculation
	@param year the year for which easter day is to be calculated
	*/
	public void setYear(int year){
		
		this.year = year;
		calcEaster(year);
	}
	
	/**
	function with the algorithm to calculate easter for a given year
	@param year the year for which easter day is to be calculated
	*/
	private void calcEaster(int year){
		
		int a = year % 19;
		int b = (int) Math.floor(year / 100);
		int c = year % 100;
		int d = (int) Math.floor(b / 4);
		int e = b % 4;
		int f = (int) Math.floor((b + 8) / 25);
		int g = (int) Math.floor((b - f + 1) / 3);
		int h = (19 * a + b - d - g + 15) % 30;
		int i = (int) Math.floor(c / 4);
		int k = c % 4;
		int l = (32 + 2 * e + 2 * i - h - k) % 7;
		int m = (int) Math.floor((a + 11 * h + 22 * l) / 451);
		this.month = (int) Math.floor((h + l - 7 * m + 114) / 31);
		this.day = ((h + l - 7 * m + 114) % 31) + 1;
	}
}
