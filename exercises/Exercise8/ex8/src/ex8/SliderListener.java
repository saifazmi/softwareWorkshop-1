package ex8;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SliderListener implements ChangeListener{
	
	private EasterModel model;
	private JSlider slider;
	
	public SliderListener(EasterModel model, JSlider slider){
		
		this.model = model;
		this.slider = slider;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		
		int value = slider.getValue();
		model.setYear(value);
	}
}
