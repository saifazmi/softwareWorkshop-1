package ex8;

public class Easter {
	
	private int day;
	private int month;
	private int year;
	
	public Easter(int year){
		
		this.year = year;
		calcEaster(year);
	}
	
	public int getDay(){
		
		return day;
	}
	
	public int getMonth(){
		
		return month;
	}
	
	public int getYear(){
		
		return year;
	}
	
	public void setYear(int year){
		
		this.year = year;
		calcEaster(year);
	}
	
	private void calcEaster(int year){
		
		int a = year % 19;
		int b = (int) Math.floor(year / 100);
		int c = year % 100;
		int d = (int) Math.floor(b / 4);
		int e = b % 4;
		int f = (int) Math.floor((b + 8) / 25);
		int g = (int) Math.floor((b - f + 1) / 3);
		int h = (19 * a + b - d - g + 15) % 30;
		int i = (int) Math.floor(c / 4);
		int k = c % 4;
		int L = (32 + 2 * e + 2 * i - h - k) % 7;
		int m = (int) Math.floor((a + 11 * h + 22 * L) / 451);
		this.month = (int) Math.floor((h + L - 7 * m + 114) / 31);
		this.day = ((h + L - 7 * m + 114) % 31) + 1;
	}
}
