package ex8;

import javax.swing.JPanel;
import javax.swing.JSlider;

public class EasterComponent extends JPanel{
	
	public EasterComponent(Easter easter, int min, int max, int initial){
		
		super();
		
		// create model object
		EasterModel model = new EasterModel(easter);
		
		// create view object
		EasterView easterView = new EasterView(model);
		
		// make views observe model
		model.addObserver(easterView);
		
		// create control
		JSlider slider = new JSlider(min, max, initial);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing((max - min) / 4);
		slider.setPaintLabels(true);
		slider.setLabelTable(slider.createStandardLabels((max - min) / 4));
		
		// create a listener object
		SliderListener listener = new SliderListener(model, slider);
		
		// make listeners listen to the right controls
		slider.addChangeListener(listener);
		
		// place views and control on panel
		add(slider);
		add(easterView);
	}
}
