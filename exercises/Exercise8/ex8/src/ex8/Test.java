package ex8;

public class Test {

	public static void main(String[] args) {
		
		Easter easter = new Easter(2001);
		System.out.println(	"Easter was on " + easter.getDay() + " " + easter.getMonth() +
							" in " + easter.getYear());
		
		easter.setYear(2015);
		System.out.println(	"Easter will be on " + easter.getDay() + " " + easter.getMonth() +
							" in " + easter.getYear());
	}

}
