package ex8;

import javax.swing.JFrame;

public class EasterGUI {

	public static void main(String[] args){
			
		JFrame frame = new JFrame("Easter day calculator");
		frame.setSize(400,150);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
		Easter easter = new Easter(2000);
		EasterComponent easterComp = new EasterComponent(	easter, // obj
															1800,	// min
															2200,	// max
															2000);	// initial
		frame.add(easterComp);
		
		frame.setVisible(true);
	}
}
